
<form enctype="multipart/form-data" method="POST" action="">
    <input type="file" name="arquivos[]" multiple="multiple" /><br><br>
    <input name="enviar" type="submit" value="Enviar">
</form>

<?php
  require 'logica/modal.php';

  if (isset($_POST['enviar'])) {
    $diretorio = "logica/arquivos";

    if (!is_dir($diretorio)) {
        echo "Pasta $diretorio nao existe";
    } else {
      $diretorio.= '/';

      $arquivos = isset($_FILES['arquivos']) ? $_FILES['arquivos'] : FALSE;

      $i = 0;
      $j = 0;
      $k = 0;

      $nomes_arquivo = array();

      for ($controle = 0; $controle < count($arquivos['name']); $controle++){
        $formatosPermitidos = array("png","jpeg","jpg", "pdf");

        $extencao = mb_strtolower(pathinfo($arquivos['name'][$controle], PATHINFO_EXTENSION));

        if (in_array($extencao, $formatosPermitidos)) {

          $temporario= $arquivos['tmp_name'][$controle];
          $novo_Nome_arquivo= uniqid().".$extencao";

          if (move_uploaded_file($temporario, $diretorio.$novo_Nome_arquivo)) {
            $nomes_arquivo[$controle] = $novo_Nome_arquivo;
            $i++;
          } else {
            $j++;
          }
        } else {
          $k++;
        }
      }

      if ($j > 0) {
        foreach ($nomes_arquivo as $value) {
          echo unlink($diretorio.$value);
        }
        modal('Não foi possível fazer o upload do arquivo','Tente novamente mais tarde ou entre em contato com o servidor responsável','641218', '0');
      } elseif ($k > 0) {
        foreach ($nomes_arquivo as $value) {
          echo unlink($diretorio.$value);
        }
        modal('Formato inválido','Somente arquivos .png .jpg .jpeg e .pdf são aceitos','641218', '0');
      } else {
        $Control -> realizar_solicitacao($id_solicitacao, $_SESSION['id'], $data_falta, strtoupper($justificativa), $nomes_arquivo, $disciplina, $id_curso_servidor, $_SESSION['id_campus'], $_SESSION['id_instituto']);
      }
    }
  }
?>
