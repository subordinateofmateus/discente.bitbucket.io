<?php
  session_start();

  require_once "logica/CRUD.class.php";

  $CRUD = new CRUD();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <script type="text/javascript">
  		function mascaraCpf(cpf){
  			if (cpf.value.length==3){
  				cpf.value=cpf.value+".";
  			} if (cpf.value.length==7){
  				cpf.value=cpf.value+".";
  			} if (cpf.value.length==11){
  				cpf.value=cpf.value+"-";
  			}
  		}
  </script>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <style type="text/css">
    .bg-register-image {
  background: url("https://source.unsplash.com/Mv9hjnEUHR4/600x800");
  background-position: center;
  background-size: cover;
}
  </style>

  <title>SB Admin 2 - Register</title>

  <!-- Custom fonts for this theme -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

  <!-- Theme CSS -->
  <link href="css/freelancer.min.css" rel="stylesheet">

</head>

<body class="bg-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900">Recuperar minha senha</h1>
                <p class="mb-4">Sua senha será enviada ao seu email</p>
              </div>
                    <form class="user" action="" method="post">
                      <?php
                        if (!isset($_SESSION['logado'])) {
                          echo "
                                <div class='form-group'>
                                  <label>Matrícula</label>
                                  <input type='text' class='form-control form-control-user' name='matricula' maxlength='14' required>
                                </div>
                          ";
                        }
                      ?>

                    <div class="form-group">
                    <label>CPF</label>
                      <input type="text" class="form-control form-control-user" id='cpf' name='cpf' onkeypress="mascaraCpf(this)" maxlength="14" required>
                      <input type="hidden" name="logado" value="1">
                    </div>
                    <button type='submit' class="btn btn-primary btn-user btn-block" name="btn_enviar_senha">
                      Enviar senha
                    </button>
                  </form>
              <hr>
              <div class="text-center">
                <a class="large" href="cadastrar.html">Crie sua conta</a>
              </div>
              <div class="text-center">
                <a class="large" href="index.html">Já possui uma conta? Entre!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>


<?php
  if (isset($_POST['btn_enviar_senha'])) {
    $logado = $_POST['logado'];

    if (isset($_POST['matricula'])) {
      $primeiro = $_POST['matricula'];
    }

    if (isset($_SESSION['logado'])) {
      $logado = $_SESSION['logado'];
      $primeiro = $_SESSION['id'];
    }

    $cpf = $_POST['cpf'];

    $CRUD -> esqueci_senha($primeiro,$cpf,$logado);
  }
?>
