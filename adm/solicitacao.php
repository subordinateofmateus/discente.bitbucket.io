<?php
  session_start();

  require '../logica/CRUD.class.php';

  $CRUD= new CRUD();

  if (isset($_GET['id'])) {
    $id = $_GET['id'];
  }
  if (isset($_GET['id_r'])) {
    $id = $_GET['id_r'];
  }
?>

<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Servidor</title>
  <link rel="stylesheet" href="../dataTable/css/dataTables.min.css">
  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
          <i><img src="../img/logo1.png" width="40" height="45"></i>
        </div>
        <div class="sidebar-brand-text mx-3">IFCE<sup>32</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="servidor.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Voltar</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!--- End of Sidebar --->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column" style="background-color: #30AD91;">

      <!-- Main Content -->
      <div id="content" style="background-color: #30AD91;">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION['nome']; ?> </span>
                <i class="fas fa-user"></i>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-lock fa-sm fa-fw mr-2 text-gray-400"></i>
                  Trocar senha
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="../logica/sair.php">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sair
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!--- End of Topbar --->


        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-center mb-4">

            <h1 class="h2 mb-0 text-white text-uppercase">SOLICITAÇÃO</h1>
          </div>

          <div class="container col-lg-12" style="background-color:transparent;">

          <div class="card text-center" >
              <?php
                if (isset($_GET['id'])) {
                  $CRUD->mostrar_dados_pedido($id);
                }
                if (isset($_GET['id_r'])) {
                    $CRUD->mostrar_dados_pedido_resolvido($id);
                }

               ?>

              </div>
          </div>

</div> <!--Container-->

</div>

</div>

<!-- Modal aceitar-->
            <div class='modal fade' id='aceitar' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
              <div class='modal-dialog' role='document'>
                <div class='modal-content'>
                  <div class='modal-header' style='background-color:#30AD91;'>
                    <h5 class='modal-title text-white' id='exampleModalLabel'>Comprovante</h5>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                      <span aria-hidden='true'>&times;</span>
                    </button>
                  </div>
                    <div class='modal-body' style="font-size: 18px;">
                      <form action="" method="POST">
                       <div class="form-group">
                        <label>Protocolo SEI</label>
                        <input type="text" name="SEI" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button style="background-color:#30AD91;" name="btn_responder_pedido_aceitar" type="submit" class="btn btn-success text-white">Aceitar</button>
                    </div>
                    </form>
                </div>
              </div>
            </div>

<!-- Modal recusar-->
            <div class='modal fade' id='recusar' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
              <div class='modal-dialog' role='document'>
                <div class='modal-content'>
                  <div class='modal-header' style='background-color:#641218;'>
                    <h5 class='modal-title text-white' id='exampleModalLabel'>Pedido recusado</h5>
                  </div>
                  <div class='modal-body text-dark' style="font-size: 18px;">
                   <form action="" method="POST">
                    <div class="form-group">
                     <label>Protocolo SEI</label>
                     <input type="text" name="SEI" class="form-control">
                     </div>
                     <div class="form-group">
                     <label>Justificativa</label>
                     <input type="text" name="justificativa_servidor" class="form-control">
                     <small id="emailHelp" class="form-text text-muted">Explique ao aluno o motivo do indeferimento</small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button style="background-color:#641218;" name="btn_responder_pedido_recusar" type="submit" class="btn btn-danger text-white">Recusar</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
<br>


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

  <script src="../dataTable/js/dataTables.min.js"></script>
</body>

</html>

<?php
  if (isset($_POST['btn_responder_pedido_recusar'])) {
    $escolha= 'recusado';

    $justificativa=$_POST['justificativa_servidor'];
  }

  if (isset($_POST['btn_responder_pedido_aceitar'])) {
    $escolha= 'aceito';

    $justificativa= 'Dados e explicação válidos!';
  }

  if (isset($_POST['btn_responder_pedido_aceitar']) || isset($_POST['btn_responder_pedido_recusar'])) {
    $situacao= $escolha;
    $SEI= $_POST['SEI'];
    $justificativa_servidor= $justificativa;

    $CRUD->responder_pedido($id, $_SESSION['id'], $SEI, $situacao, $justificativa_servidor);
  }
?>
<script>
    $(document).ready(function(){
      $('table').DataTable(
        {"lengthMenu" : [ [ 10, 15, 25, 50, -1 ], [ 10, 15, 25, 50, "Todos" ] ],
        "order" : [[3 ,'desc']],
        responsive : true,
        "language" : {
        "sEmptyTable" : "Nenhum registro encontrado",
        "sInfo" : "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty" : "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered" : "(Filtrados de _MAX_ registros)",
        "sInfoPostFix" : "",
        "sInfoThousands" : ".",
        "sLengthMenu" : "_MENU_ resultados por página",
        "sLoadingRecords" : "Carregando...",
        "sProcessing" : "Processando...",
        "sZeroRecords" : "Nenhum registro encontrado",
        "sSearch" : "Pesquisar",
        "oPaginate" : {
        "sNext" : "<i class='fas fa-angle-double-right'></i>",
        "sPrevious" : "<i class='fas fa-angle-double-left'></i>",
        "sFirst" : "Primeiro",
        "sLast" : "Último",
        },
        "oAria" : {
        "sSortAscending" : ": Ordenar colunas de forma ascendente",
        "sSortDescending" : ": Ordenar colunas de forma descendente"
        }
        }
          });
    });
    </script>
