<?php
  session_start();

  require '../logica/CRUD.class.php';

  $CRUD= new CRUD();
?>

<!DOCTYPE html>
<html>
<head>

  <script>
      function mascaraCpf(cpf){
        if (cpf.value.length==3){
          cpf.value=cpf.value+".";
        } if (cpf.value.length==7){
          cpf.value=cpf.value+".";
        } if (cpf.value.length==11){
          cpf.value=cpf.value+"-";
        }
      }

      function mascaraRG(rg){
        if (rg.value.length==8){
          rg.value=rg.value+"-";
        }
      }

      function mascaraFone(fone){
        if(fone.value.length==1)
          fone.value="("+fone.value;
        if(fone.value.length==3)
          fone.value=fone.value+") ";
        if(fone.value.length==4)
          fone.value=fone.value+" ";
        if(fone.value.length==6)
          fone.value=fone.value+" ";
        if(fone.value.length==11)
          fone.value=fone.value+"-";
      }

      function mascaraCEP(cep){
        if(cep.value.length==5)
          cep.value=cep.value+"-";
      }
  </script>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Administrador PD</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="admin.php">
        <div class="sidebar-brand-icon">
          <i><img src="../img/logo1.png" width="40" height="45"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Admin<sup>32</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
        Solicitações
      </div>
      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="admin.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Geral</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Alunos
      </div>

      <!-- Nav Item - adc alunos -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="aluno.php">
          <i class="fas fa-fw fa-plus"></i>
          <span>Adicionar alunos</span>
        </a>
      </li>

      <!-- Nav Item - no sistema -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="aluno.php#sistema">
          <i class="fas fa-fw fa-table"></i>
          <span>Alunos no sistema</span>
        </a>
      </li>

      <!-- Nav Item - no campus -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="aluno.php#campus">
          <i class="fas fa-fw fa-home"></i>
          <span>Alunos no Campus</span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Servidores
      </div>

      <!-- Nav Item - adc alunos -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="serv.php">
          <i class="fas fa-fw fa-plus"></i>
          <span>Adicionar servidores</span>
        </a>
      </li>

      <!-- Nav Item - no sistema -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="serv.php#serv_cad">
          <i class="fas fa-fw fa-table"></i>
          <span>Servidores cadastrados</span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Cursos
      </div>

      <!-- Nav Item - adc alunos -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="curso.php">
          <i class="fas fa-fw fa-plus"></i>
          <span>Adicionar cursos</span>
        </a>
      </li>

      <!-- Nav Item - no sistema -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="curso.php#cad">
          <i class="fas fa-fw fa-table"></i>
          <span>Cursos cadastrados</span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
                <span class="badge badge-danger badge-counter">3+</span>
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Alerts Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-primary">
                      <i class="fas fa-file-alt text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 12, 2019</div>
                    <span class="font-weight-bold">A new monthly report is ready to download!</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-success">
                      <i class="fas fa-donate text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 7, 2019</div>
                    $290.29 has been deposited into your account!
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-warning">
                      <i class="fas fa-exclamation-triangle text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 2, 2019</div>
                    Spending Alert: We've noticed unusually high spending for your account.
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
              </div>
            </li>

            <!-- Nav Item - Messages -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i>
                <!-- Counter - Messages -->
                <span class="badge badge-danger badge-counter">7</span>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Message Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div class="font-weight-bold">
                    <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                    <div class="small text-gray-500">Emily Fowler · 58m</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                    <div class="status-indicator"></div>
                  </div>
                  <div>
                    <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
                    <div class="small text-gray-500">Jae Chun · 1d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                    <div class="status-indicator bg-warning"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
                    <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
                    <div class="small text-gray-500">Chicken the Dog · 2w</div>
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Administrador</span>
                <i class="fas fa-user"></i>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-dark text-uppercase">Alunos</h1>
          </div>

            <!-- Adicionar aluno -->
          <div class="card shadow mb-4" id="adc">
            <div class="card-header py-3 bg-success">
              <h5 class="m-0 font-weight-bold text-white ">Adicionar aluno</h5>
            </div>
            <div class="card-body text-dark" style="font-size:18px;">
              <form action="" method="POST">
                  <div class="form-row">
                      <div class="form-group col-md-4">
                      <label>Matrícula</label>
                      <input type="text" name="matricula" class="form-control">
                      </div>
                      <div class="form-group col-md-6">
                      <label>Nome</label>
                      <input type="text" name="nome" class="form-control">
                      </div>
                      <div class="form-group col-md-2">
                      <label>Semestre</label>
                      <input type="text" name="semestre" class="form-control">
                      </div>
                  </div>
                  <div class="form-row">
                      <div class="form-group col-md-4">
                        <label>RG</label>
                        <input type="text" id="rg" name="rg" onkeypress="mascaraRG(this)" placeholder='xxxxxxxx-x' maxlength="10" class="form-control">
                      </div>
                      <div class="form-group col-md-4">
                        <label>CPF</label>
                        <input type="text" id="cpf" name="cpf" placeholder='xxx.xxx.xxx-xx' onkeypress="mascaraCpf(this)" maxlength="14" class="form-control">
                      </div>
                      <div class="form-group col-md-4">
                        <label>Data de Nascimento</label>
                        <input type="date" name="data_nascimento" id="" class="form-control">
                      </div>

                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-2">
                      <label>CEP</label>
                      <input type="text" id="cep" name="cep" onKeyPress="mascaraCEP(this)" maxlength="9" placeholder="xxxxx-xxx" class="form-control">
                    </div>
                    <div class="form-group col-md-4">
                      <label for="">Cidade</label>
                      <input type="text" id='cidade' name="cidade" class="form-control">
                    </div>
                    <div class="form-group col-md-2">
                      <label for="">Estado</label>
                      <select class="custom-select" name="estado" id="uf">
                  			<option value="AC">Acre</option>
                  			<option value="AL">Alagoas</option>
                  			<option value="AP">Amapá</option>
                  			<option value="AM">Amazonas</option>
                  			<option value="BA">Bahia</option>
                  			<option value="CE">Ceará</option>
                  			<option value="DF">Distrito Federal</option>
                  			<option value="ES">Espírito Santo</option>
                  			<option value="GO">Goiás</option>
                  			<option value="MA">Maranhão</option>
                  			<option value="MT">Mato Grosso</option>
                  			<option value="MS">Mato Grosso do Sul</option>
                  			<option value="MG">Minas Gerais</option>
                  			<option value="PA">Pará</option>
                  			<option value="PB">Paraíba</option>
                  			<option value="PR">Paraná</option>
                  			<option value="PE">Pernambuco</option>
                  			<option value="PI">Piauí</option>
                  			<option value="RJ">Rio de Janeiro</option>
                  			<option value="RN">Rio Grande do Norte</option>
                  			<option value="RS">Rio Grande do Sul</option>
                  			<option value="RO">Rondônia</option>
                  			<option value="RR">Roraima</option>
                  			<option value="SC">Santa Catarina</option>
                  			<option value="SP">São Paulo</option>
                  			<option value="SE">Sergipe</option>
                  			<option value="TO">Tocantins</option>
                  		</select>
                    </div>
                    <div class="form-group col-md-4">
                      <label>Telefone</label>
                      <input type="text" id="fone" name="fone" onKeyPress="mascaraFone(this)" maxlength="16" placeholder="(xx) x xxxx-xxxx" class="form-control">
                    </div>
                  </div>
                  <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="inputAddress">Bairro</label>
                        <input type="text" class="form-control" name="bairro" id="bairro">
                      </div>
                      <div class="form-group col-md-7">
                        <label for="inputAddress">Rua</label>
                        <input type="text" class="form-control" name="rua" id="logradouro">
                      </div>
                      <div class="form-group col-md-1">
                        <label for="inputAddress">Número</label>
                        <input type="text" class="form-control" name="numero" id="numero">
                      </div>
                  </div>

                  <button type="submit" name="btn_Cadastrar_aluno" class="btn btn-success">Cadastrar</button>
                </form>
            </div>
          </div>
          <br>
          <!-- Alunos no campus -->
          <div class="card shadow mb-4" id="bloco">
            <div class="card-header py-3 bg-success">
              <h5 class="m-0 font-weight-bold text-white ">Cadastrar em bloco</h5>
            </div>
            <div class="card-body text-dark">
              <form class="" action="" method="post">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline1" name="opc_cadastro" value='1' class="custom-control-input">
                  <label class="custom-control-label text-primary mb-3" for="customRadioInline1" style="font-size:18px;">Adicionar turma</label>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                    <label>Curso</label>
                    <select class="custom-select" name="curso">
                      <?php
                        $CRUD -> select_cursos();
                      ?>
                    </select>
                    </div>
                    <div class="form-group col-md-4">
                    <label>Semestre</label>
                    <input type="text" class="form-control" name="semestre" placeholder="2019.3">
                    </div>
                </div>
                <label for="">Arquivo CSV com todos os dados</label>
                <div class="custom-file">
                  <input type="file" class="custom-file-input " id="customFile" name='alunos'>
                  <label class="custom-file-label" for="customFile">Escolher arquivo</label>
                </div>

              <hr>

              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="customRadioInline2" name="opc_cadastro" class="custom-control-input">
                <label class="custom-control-label mb-3 text-primary" for="customRadioInline2" style="font-size:18px;">Adicionar todos os alunos do Campus</label>
              </div>

                <div class="form-group">
                <label for="">Arquivo CSV com todos os dados</label>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" name='alunos' id="customFile">
                  <label class="custom-file-label" for="customFile">Escolher arquivo</label>
                </div>
                </div>
                <br>
                <button type="submit" class="btn btn-success" name="btn_cadastrar_csv_alunos">
                    Cadastrar
                </button>
              </form>
            </div>
          </div>
<br>
          <!-- Alunos no campus -->
          <div class="card shadow mb-4" id="sistema">
            <div class="card-header py-3 bg-success">
              <h5 class="m-0 font-weight-bold text-white ">Alunos no sistema</h5>
            </div>
            <div class="card-body text-dark">
              <div class="table-responsive">
                <table class="table table-striped text-center text-dark" id="catalogo2">
                    <thead>
                <tr>
                      <th scope="col">Matrícula</th>
                      <th scope="col">Aluno</th>
                      <th scope="col">Email</th>
                      <th scope="col">Curso</th>

                </tr>
                </thead>
                    <tbody>
                        <?php
                         $CRUD->mostrar_alunos_cadastrados();
                        ?>
                    </tbody>
                  </table>

                </div> <!--responsiva-->
            </div>
          </div>
<br>
          <!-- Alunos no Campus -->
          <div class="card shadow mb-4" id="campus">
            <div class="card-header py-3 bg-success">
              <h5 class="m-0 font-weight-bold text-white ">Alunos no Campus</h5>
            </div>
            <div class="card-body text-dark">
              <div class="table-responsive">
                <table class="table table-striped text-center text-dark" id="catalogo2">
                  <thead>
                    <tr>
                      <th scope="col">Matrícula</th>
                      <th scope="col">Nome</th>
                      <th scope="col">RG</th>
                      <th scope="col">CPF</th>
                      <th scope="col">Nascimento</th>
                      <th scope="col">Telefone</th>
                      <th scope="col">Endereço</th>
                      <th scope="col">CEP</th>
                      <th scope="col">Cidade</th>
                      <th scope="col">Estado</th>

                    </tr>
                  </thead>
                  <tbody>
                     <?php
                       $CRUD->mostrar_alunos_campos();
                      ?>
                  </tbody>
                  </table>
                          </div> <!--responsiva-->
                      </div>
                    </div>

<br>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

</body>
<script type="text/javascript">
    $("#cep").focusout(function(){
      $.ajax({
        url: 'https://viacep.com.br/ws/'+$(this).val()+'/json/unicode/',
        dataType: 'json',
        success: function(resposta){
          $("#logradouro").val(resposta.logradouro);
          $("#bairro").val(resposta.bairro);
          $("#cidade").val(resposta.localidade);
          $("#uf").val(resposta.uf);
          $("#numero").focus();
        }
      });
    });

		$(document).ready(function(){
			$('table').DataTable(
				{"lengthMenu" : [ [ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "Todos" ] ],
			  "order" : [[3 ,'asc']],
			  responsive : true,
			  "language" : {
			  "sEmptyTable" : "Nenhum registro encontrado",
			  "sInfo" : "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			  "sInfoEmpty" : "Mostrando 0 até 0 de 0 registros",
			  "sInfoFiltered" : "(Filtrados de _MAX_ registros)",
			  "sInfoPostFix" : "",
			  "sInfoThousands" : ".",
			  "sLengthMenu" : "_MENU_ resultados por página",
			  "sLoadingRecords" : "Carregando...",
			  "sProcessing" : "Processando...",
			  "sZeroRecords" : "Nenhum registro encontrado",
			  "sSearch" : "Pesquisar",
			  "oPaginate" : {
			  "sNext" : "<i class='fas fa-angle-double-right'></i>",
			  "sPrevious" : "<i class='fas fa-angle-double-left'></i>",
			  "sFirst" : "Primeiro",
			  "sLast" : "Último",
			  },
			  "oAria" : {
			  "sSortAscending" : ": Ordenar colunas de forma ascendente",
			  "sSortDescending" : ": Ordenar colunas de forma descendente"
			  }
			  }
					});
		});
		</script>
</html>

<?php
  if (isset($_POST['btn_Cadastrar_aluno'])) {
    $matricula = $_POST['matricula'];
    $rg = $_POST['rg'];
    $cpf = $_POST['cpf'];
    $semestre_insercao = $_POST['semestre'];
    $data_nascimento = $_POST['data_nascimento'];
    $cep = $_POST['cep'];
    $cidade = $_POST['cidade'];
    $estado = $_POST['estado'];
    $fone = $_POST['fone'];
    $endereco = $_POST['rua'].' '.$_POST['numero'].', '.$_POST['bairro'];

    $CRUD = cadastrar_aluno($matricula, $nome, $semestre_insercao, $rg, $cpf, $data_nascimento, $fone, $endereco, $cidade, $estado, $cep);


    }
?>
