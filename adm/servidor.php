<?php
  session_start();

  require '../logica/CRUD.class.php';

  $CRUD= new CRUD();
?>

<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Servidor</title>
  <link rel="stylesheet" href="../dataTable/css/dataTables.min.css">
  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
          <i><img src="../img/logo1.png" width="40" height="45"></i>
        </div>
        <div class="sidebar-brand-text mx-3">IFCE<sup>32</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="">
          <i class="fas fa-fw fa-table"></i>
          <span>Geral</span></a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-------------------------------------------- End of Sidebar -------------------------------------------->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column" style="background-color: #30AD91;">

      <!-- Main Content -->
      <div id="content" style="background-color: #30AD91;">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
                <span class="badge badge-danger badge-counter">3+</span>
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Alerts Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-primary">
                      <i class="fas fa-file-alt text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 12, 2019</div>
                    <span class="font-weight-bold">A new monthly report is ready to download!</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-success">
                      <i class="fas fa-donate text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 7, 2019</div>
                    $290.29 has been deposited into your account!
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-warning">
                      <i class="fas fa-exclamation-triangle text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 2, 2019</div>
                    Spending Alert: We've noticed unusually high spending for your account.
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
              </div>
            </li>

            <!-- Nav Item - Messages -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i>
                <!-- Counter - Messages -->
                <span class="badge badge-danger badge-counter">7</span>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Message Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div class="font-weight-bold">
                    <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                    <div class="small text-gray-500">Emily Fowler · 58m</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                    <div class="status-indicator"></div>
                  </div>
                  <div>
                    <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
                    <div class="small text-gray-500">Jae Chun · 1d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                    <div class="status-indicator bg-warning"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
                    <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
                    <div class="small text-gray-500">Chicken the Dog · 2w</div>
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION['nome']; ?> </span>
                <i class="fas fa-user"></i>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="../trocar_senha.php">
                  <i class="fas fa-lock fa-sm fa-fw mr-2 text-gray-400"></i>
                  Trocar senha
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="../logica/sair.php">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sair
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!----------------------------------------- End of Topbar ----------------------------------------->


        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-white text-uppercase">Solicitações gerais</h1>
          </div>

          <div class="container col-lg-12" style="border: 1px solid #D3D3D3; background-color:whitesmoke; border-radius: 3px; ">

  <div class="row justify-content-md-center">

    <div class="col-md-2">
      <nav class="nav flex-column text-justify">

        <a class="nav-link disabled lead text-dark font-weight-bold"  href="#" tabindex="-1"
        aria-disabled="true">
          <i class="fas fa-bars"></i> MENU
        </a>

        <!------------------------Menu de livros----------------------------------->
          <a class="nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">PENDENTES</a>

          <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">CONCLUÍDOS</a>
      </nav>
    </div>

    <div class="col-md-10"  style="border-left:1px solid #D3D3D3;">

      <!-------------------------------------TROCA-------------------------------------------------------->

      <div class="tab-content" id="nav-tabContent">

        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

      <br>
      <h2 class="text-dark text-center">PENDENTES</h2> <br>


      <!-------------------------------PESQUISA LIVRO---------------------------------------------->

        <div class="table-responsive">
          <table class="table table-hover text-center text-dark" id="catalogo2">
            <thead>
              <tr>
                <th scope="col">Pedido</th>
                <th scope="col">Aluno</th>
                <th scope="col">Solicitação</th>
                <th scope="col">Data/Hora</th>
              </tr>
            </thead>
            <tbody>
               <?php
                 $CRUD->select_pedidos_pendentes_servidor($_SESSION['id_setor_campus'], $_SESSION['id_curso_campus']);
                ?>
            </tbody>
          </table>

        </div> <!--responsiva-->

      </div>


      <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

        <br>
      <h2 class="text-center">CONCLUÍDOS</h2> <br>

          <table class="table table-hover text-center text-dark" id="catalogo2">
            <thead>
        <tr>
        <th scope="col">Pedido</th>
              <th scope="col">Aluno</th>
              <th scope="col">Solicitação</th>
              <th scope="col">Data/Hora</th>
              <th scope="col">Protocolo</th>
              <th scope="col">Situação</th>
        </tr>
        </thead>
            <tbody>
      <?php
                 $CRUD->select_pedidos_resolvidos_servidor($_SESSION['id'], $_SESSION['id_curso']);
                ?>
            </tbody>
          </table>
        </div>

      </div>

    </div> <!--Col-->

  </div> <!--Row-->

</div> <!--Container-->

</div>

</div>


<br>


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

  <script src="../dataTable/js/dataTables.min.js"></script>
</body>

</html>
<script>
    $(document).ready(function(){
      $('table').DataTable(
        {"lengthMenu" : [ [ 10, 15, 25, 50, -1 ], [ 10, 15, 25, 50, "Todos" ] ],
        "order" : [[3 ,'desc']],
        responsive : true,
        "language" : {
        "sEmptyTable" : "Nenhum registro encontrado",
        "sInfo" : "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty" : "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered" : "(Filtrados de _MAX_ registros)",
        "sInfoPostFix" : "",
        "sInfoThousands" : ".",
        "sLengthMenu" : "_MENU_ resultados por página",
        "sLoadingRecords" : "Carregando...",
        "sProcessing" : "Processando...",
        "sZeroRecords" : "Nenhum registro encontrado",
        "sSearch" : "Pesquisar",
        "oPaginate" : {
        "sNext" : "<i class='fas fa-angle-double-right'></i>",
        "sPrevious" : "<i class='fas fa-angle-double-left'></i>",
        "sFirst" : "Primeiro",
        "sLast" : "Último",
        },
        "oAria" : {
        "sSortAscending" : ": Ordenar colunas de forma ascendente",
        "sSortDescending" : ": Ordenar colunas de forma descendente"
        }
        }
          });
    });
    </script>
