<?php
  session_start();

  require_once 'logica/CRUD.class.php';

  $CRUD = new CRUD;

  if ($_SESSION['logado'] == 1) {
    $pagina = "menu";
  } else {
    $pagina = "adm/servidor";
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <style type="text/css">
    .bg-register-image {
  background: url("https://source.unsplash.com/Mv9hjnEUHR4/600x800");
  background-position: center;
  background-size: cover;
}
  </style>

  <title>SB Admin 2 - Register</title>

  <!-- Custom fonts for this theme -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

  <!-- Theme CSS -->
  <link href="css/freelancer.min.css" rel="stylesheet">

</head>

<body class="bg-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block"> <img src="img/thumb/senha.jpg" style="height:100%;"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h2 mb-3 text-gray-900">Trocar senha</h1>
              </div>
                    <form style="font-size:14px;" action="" method="post">
                      <div class="control-group">
                        <div class="form-group floating-label-form-group controls mb-3 pb-1">
                          <label>Senha atual</label>
                          <input class="form-control text-uppercase" type="password" placeholder="Senha atual" name="senha_atual" required="required">
                        </div>
                      </div>
                    <div class="control-group">
                      <div class="form-group floating-label-form-group controls mb-3 pb-1">
                        <label>Nova senha</label>
                        <input class="form-control text-uppercase" name="senha_nova" type="password" placeholder="Nova senha" required="required" data-validation-required-message="Please enter your name.">
                      </div>
                    </div>
                    <div class="control-group">
                      <div class="form-group floating-label-form-group controls mb-3 pb-1">
                        <label>Confirmar senha</label>
                        <input class="form-control text-uppercase" name="senha_confirmar" type="password" placeholder="Confirmar senha" required="required" data-validation-required-message="Please enter your name.">
                      </div>
                    </div>
                    <button name='btn_confirmar' class="btn btn-primary btn-user btn-block">
                      Confirmar
                    </button>
                  </form>
              <hr>
              <div class="text-center">
                <a class="large" href="<?php echo $pagina; ?>.php">Voltar</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>


<?php
  if (isset($_POST['btn_confirmar'])) {
    $senha_atual = $_POST['senha_atual'];
    $senha_nova = $_POST['senha_nova'];
    $senha_confirmar = $_POST['senha_confirmar'];

    if ($senha_atual == $_SESSION['senha']) {
      if ($senha_nova == $senha_confirmar) {
        $CRUD -> alterar_senha($_SESSION['id'], $senha_nova, $_SESSION['logado']);
      } else {
        modal('Senhas incoerrentes', 'Digite as', '641218', '0');
      }
    } else {
      modal('Senha inválida', 'Digite sua senha corretamente', '641218', '0');
    }
  }

?>
