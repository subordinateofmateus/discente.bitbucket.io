-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 21-Ago-2019 às 19:06
-- Versão do servidor: 10.3.16-MariaDB
-- versão do PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `projeto_discente`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `adm`
--

CREATE TABLE `adm` (
  `id_adm` int(11) NOT NULL,
  `nome_adm` varchar(50) DEFAULT NULL,
  `senha` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `adm`
--

INSERT INTO `adm` (`id_adm`, `nome_adm`, `senha`) VALUES
(1, 'NOIZ', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos`
--

CREATE TABLE `cursos` (
  `id_curso` int(11) NOT NULL,
  `nome_curso` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cursos`
--

INSERT INTO `cursos` (`id_curso`, `nome_curso`) VALUES
(0, 'SEM CURSO'),
(1, 'Tecnico de Informáti'),
(2, 'Engenharia de Produç'),
(3, 'Matemática');

-- --------------------------------------------------------

--
-- Estrutura da tabela `discentes`
--

CREATE TABLE `discentes` (
  `id_discente` int(11) NOT NULL,
  `nome_discente` varchar(50) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `senha` varchar(12) NOT NULL,
  `cpf` char(14) DEFAULT NULL,
  `matricula` char(14) DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `id_curso_fk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `discentes`
--

INSERT INTO `discentes` (`id_discente`, `nome_discente`, `email`, `senha`, `cpf`, `matricula`, `data_nascimento`, `id_curso_fk`) VALUES
(1, 'Kelvin Gomes Fernandes', 'kelpgf@gmail.com', '2', '086.466.413-33', '20192032123452', '2002-06-16', 1),
(2, 'Christian Paulo', 'christian.paulo54@gmail.com', 'IF3220193415', '654.215.656-42', '20192432134522', '2001-10-13', 1),
(3, 'Luanna Barros Soares', 'luannabs169@gmail.com', 'IF3220191019', '122.434.234-23', '20192032149852', '2001-09-05', 1),
(4, 'Rafael Cachorro Salcicha', 'rafaelpfvr@gmail.com', 'IF3220190719', '000.000.000-00', '20192098712345', '2001-08-22', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `materias`
--

CREATE TABLE `materias` (
  `id_materia` int(11) NOT NULL,
  `id_curso_fk` int(11) DEFAULT NULL,
  `nome_materia` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `materias`
--

INSERT INTO `materias` (`id_materia`, `id_curso_fk`, `nome_materia`) VALUES
(1, 1, 'Lógica de Programção'),
(2, 1, 'PHP'),
(3, 1, 'HTML');

-- --------------------------------------------------------

--
-- Estrutura da tabela `matriculados`
--

CREATE TABLE `matriculados` (
  `matricula` char(14) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `matriculados`
--

INSERT INTO `matriculados` (`matricula`, `nome`, `data_nascimento`) VALUES
('20192032123452', 'Kelvin Gomes Fernandes', '2002-06-16'),
('20192032149852', 'Luanna Barros Soares', '2001-09-05'),
('20192098712345', 'Rafael Cachorro Salcicha', '2001-08-22'),
('20192432134522', 'Christian Paulo', '2001-10-13');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedidos_pendentes`
--

CREATE TABLE `pedidos_pendentes` (
  `id_pedidos_pendentes` int(11) NOT NULL,
  `id_solicitacao_fk` int(11) DEFAULT NULL,
  `id_discente_fk` int(11) DEFAULT NULL,
  `id_curso_fk` int(11) NOT NULL,
  `data` date DEFAULT NULL,
  `justificativa` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `id_servidor_fk` int(11) DEFAULT NULL,
  `arquivo` varchar(999) NOT NULL,
  `id_materia_fk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pedidos_pendentes`
--

INSERT INTO `pedidos_pendentes` (`id_pedidos_pendentes`, `id_solicitacao_fk`, `id_discente_fk`, `id_curso_fk`, `data`, `justificativa`, `status`, `id_servidor_fk`, `arquivo`, `id_materia_fk`) VALUES
(1, 1, 1, 1, '2019-08-16', 'Pq eu quero', 1, 6, '', NULL),
(2, 2, 1, 2, '2019-07-15', 'sla', 1, 6, '', NULL),
(3, 1, 1, 1, '2019-08-19', 'ertyuil', 0, 7, '5d5c55aceb8eb.png', NULL),
(8, 1, 1, 1, '2019-08-21', 'werty', 0, 7, '5d5d78c3eb51b.png', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedidos_resolvidos`
--

CREATE TABLE `pedidos_resolvidos` (
  `id_pedidos_resolvidos` int(11) NOT NULL,
  `id_pedidos_pendente_fk` int(11) DEFAULT NULL,
  `numero_SEI` int(11) DEFAULT NULL,
  `data_hora` datetime NOT NULL,
  `justificativa_servidor` varchar(200) NOT NULL,
  `situacao` enum('aceito','recusado') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pedidos_resolvidos`
--

INSERT INTO `pedidos_resolvidos` (`id_pedidos_resolvidos`, `id_pedidos_pendente_fk`, `numero_SEI`, `data_hora`, `justificativa_servidor`, `situacao`) VALUES
(1, 1, 413343132, '2019-09-12 15:21:38', 'SEM NEXO', 'recusado'),
(5, 2, 1232431231, '2019-08-20 02:38:43', 'pq ss', 'recusado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servidores`
--

CREATE TABLE `servidores` (
  `id_servidor` int(11) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `nome_servidor` varchar(50) DEFAULT NULL,
  `cpf` char(14) NOT NULL,
  `email_servidor` varchar(40) DEFAULT NULL,
  `id_setor_fk` int(11) DEFAULT NULL,
  `id_curso_fk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `servidores`
--

INSERT INTO `servidores` (`id_servidor`, `senha`, `nome_servidor`, `cpf`, `email_servidor`, `id_setor_fk`, `id_curso_fk`) VALUES
(2, '123', 'Matheus', '', 'mat@gmail.com', 1, 1),
(4, '321', 'Jusssimara', '', 'ju@gmail.com', 4, 0),
(6, '1', 'Kelvin Gomes Fernandes', '086.466.413-33', 'kelpgf@gmail.com', 1, 0),
(7, 'IF32S201900192008084', 'Lulus', '056.345.678-35', 'luannabs169@gmail.com', 2, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `setores`
--

CREATE TABLE `setores` (
  `id_setor` int(11) NOT NULL,
  `nome_setor` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `setores`
--

INSERT INTO `setores` (`id_setor`, `nome_setor`) VALUES
(1, 'Coordenação do Curso'),
(2, 'Coordenação Técnico Pedagógica'),
(4, 'Coordenaria de Controle Acadêmico'),
(5, 'Recepção do Campos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitacoes`
--

CREATE TABLE `solicitacoes` (
  `id_solicitacao` int(11) NOT NULL,
  `titulo_solicitacao` varchar(100) DEFAULT NULL,
  `id_setor_fk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `solicitacoes`
--

INSERT INTO `solicitacoes` (`id_solicitacao`, `titulo_solicitacao`, `id_setor_fk`) VALUES
(1, 'Cancelamento de matrícula', 2),
(2, 'Colação de grau especial', 5),
(3, 'Justificativa de falta dos dias', 1),
(4, 'Segunda chamada da prova', 1),
(5, 'Trancamento da disciplina', 1),
(6, 'Trancamento da matrícula', 2);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `adm`
--
ALTER TABLE `adm`
  ADD PRIMARY KEY (`id_adm`);

--
-- Índices para tabela `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id_curso`);

--
-- Índices para tabela `discentes`
--
ALTER TABLE `discentes`
  ADD PRIMARY KEY (`id_discente`),
  ADD UNIQUE KEY `cpf` (`cpf`),
  ADD UNIQUE KEY `matricula` (`matricula`),
  ADD KEY `id_curso_fk` (`id_curso_fk`);

--
-- Índices para tabela `materias`
--
ALTER TABLE `materias`
  ADD PRIMARY KEY (`id_materia`),
  ADD KEY `id_curso_fk` (`id_curso_fk`);

--
-- Índices para tabela `matriculados`
--
ALTER TABLE `matriculados`
  ADD PRIMARY KEY (`matricula`);

--
-- Índices para tabela `pedidos_pendentes`
--
ALTER TABLE `pedidos_pendentes`
  ADD PRIMARY KEY (`id_pedidos_pendentes`),
  ADD KEY `id_solicitacao_fk` (`id_solicitacao_fk`),
  ADD KEY `id_dicente_fk` (`id_discente_fk`),
  ADD KEY `id_curso_fk` (`id_curso_fk`),
  ADD KEY `id_servidor_fk` (`id_servidor_fk`),
  ADD KEY `disciplina` (`id_materia_fk`);

--
-- Índices para tabela `pedidos_resolvidos`
--
ALTER TABLE `pedidos_resolvidos`
  ADD PRIMARY KEY (`id_pedidos_resolvidos`),
  ADD KEY `id_pedido_pendente_fk` (`id_pedidos_pendente_fk`);

--
-- Índices para tabela `servidores`
--
ALTER TABLE `servidores`
  ADD PRIMARY KEY (`id_servidor`),
  ADD KEY `id_setor_fk` (`id_setor_fk`),
  ADD KEY `id_cargo_fk` (`id_curso_fk`);

--
-- Índices para tabela `setores`
--
ALTER TABLE `setores`
  ADD PRIMARY KEY (`id_setor`);

--
-- Índices para tabela `solicitacoes`
--
ALTER TABLE `solicitacoes`
  ADD PRIMARY KEY (`id_solicitacao`),
  ADD KEY `id_setor_fk` (`id_setor_fk`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `adm`
--
ALTER TABLE `adm`
  MODIFY `id_adm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id_curso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `discentes`
--
ALTER TABLE `discentes`
  MODIFY `id_discente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `materias`
--
ALTER TABLE `materias`
  MODIFY `id_materia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `pedidos_pendentes`
--
ALTER TABLE `pedidos_pendentes`
  MODIFY `id_pedidos_pendentes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `pedidos_resolvidos`
--
ALTER TABLE `pedidos_resolvidos`
  MODIFY `id_pedidos_resolvidos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `servidores`
--
ALTER TABLE `servidores`
  MODIFY `id_servidor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `setores`
--
ALTER TABLE `setores`
  MODIFY `id_setor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `solicitacoes`
--
ALTER TABLE `solicitacoes`
  MODIFY `id_solicitacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `discentes`
--
ALTER TABLE `discentes`
  ADD CONSTRAINT `discentes_ibfk_1` FOREIGN KEY (`id_curso_fk`) REFERENCES `cursos` (`id_curso`),
  ADD CONSTRAINT `discentes_ibfk_2` FOREIGN KEY (`matricula`) REFERENCES `matriculados` (`matricula`);

--
-- Limitadores para a tabela `materias`
--
ALTER TABLE `materias`
  ADD CONSTRAINT `materias_ibfk_1` FOREIGN KEY (`id_curso_fk`) REFERENCES `cursos` (`id_curso`);

--
-- Limitadores para a tabela `pedidos_pendentes`
--
ALTER TABLE `pedidos_pendentes`
  ADD CONSTRAINT `pedidos_pendentes_ibfk_1` FOREIGN KEY (`id_solicitacao_fk`) REFERENCES `solicitacoes` (`id_solicitacao`),
  ADD CONSTRAINT `pedidos_pendentes_ibfk_2` FOREIGN KEY (`id_discente_fk`) REFERENCES `discentes` (`id_discente`),
  ADD CONSTRAINT `pedidos_pendentes_ibfk_3` FOREIGN KEY (`id_curso_fk`) REFERENCES `cursos` (`id_curso`),
  ADD CONSTRAINT `pedidos_pendentes_ibfk_4` FOREIGN KEY (`id_servidor_fk`) REFERENCES `servidores` (`id_servidor`),
  ADD CONSTRAINT `pedidos_pendentes_ibfk_5` FOREIGN KEY (`id_materia_fk`) REFERENCES `materias` (`id_materia`);

--
-- Limitadores para a tabela `pedidos_resolvidos`
--
ALTER TABLE `pedidos_resolvidos`
  ADD CONSTRAINT `pedidos_resolvidos_ibfk_1` FOREIGN KEY (`id_pedidos_pendente_fk`) REFERENCES `pedidos_pendentes` (`id_pedidos_pendentes`);

--
-- Limitadores para a tabela `servidores`
--
ALTER TABLE `servidores`
  ADD CONSTRAINT `servidores_ibfk_1` FOREIGN KEY (`id_setor_fk`) REFERENCES `setores` (`id_setor`),
  ADD CONSTRAINT `servidores_ibfk_2` FOREIGN KEY (`id_curso_fk`) REFERENCES `cursos` (`id_curso`);

--
-- Limitadores para a tabela `solicitacoes`
--
ALTER TABLE `solicitacoes`
  ADD CONSTRAINT `solicitacoes_ibfk_1` FOREIGN KEY (`id_setor_fk`) REFERENCES `setores` (`id_setor`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
