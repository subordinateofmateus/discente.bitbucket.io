<?php  
	session_start();

	require 'CRUD.class.php';

	$CRUD= new CRUD();
?>

<script type="text/javascript">
		function mascaraCpf(cpf){
			if (cpf.value.length==3){
				cpf.value=cpf.value+".";
			} if (cpf.value.length==7){
				cpf.value=cpf.value+".";
			} if (cpf.value.length==11){
				cpf.value=cpf.value+"-";
			}
		}
	</script>

<table>
	<tr>
		<td>Pedido</td>
		<td>Aluno</td>
		<td>Solicitação</td>
		<td>Data/Hora</td>
		<td>Servidor</td>
		<td>Setor</td>
	</tr>
	<?php  
		$CRUD->select_pedidos_pendentes();		
	?>
</table>

<br>

<table>
	<tr>
		<td>Pedido</td>
		<td>Aluno</td>
		<td>Solicitação</td>
		<td>Data/Hora</td>
		<td>Servidor</td>
		<td>SEI</td>
		<td>Situação</td>
	</tr>
	<?php  
		$CRUD->select_pedidos_resolvidos();		
	?>
</table>
<br>


cadastrar servidor
<form action="" method='POST'>
	<input type="text" name="nome">
	<input type="email" name="email">
	<input type="text" name="cpf" id="cpf" placeholder='xxx.xxx.xxx-xx' onkeypress="mascaraCpf(this)" maxlength="14">
	<select name='setor'>
	<?php 
		$CRUD->select_setores();
	?>		
	</select>

	<select name='curso'>
		<option value='0'>Nenhum</option>
	<?php 
		$CRUD->select_cursos();
	?>		
	</select>

	<input type="submit" name="btn_cadastrar_servidor" value='Cadastrar'>
</form>

<br>


<?php  
	if (isset($_POST['btn_cadastrar_servidor'])) {
		$nome= $_POST['nome'];
		$email= $_POST['email'];
		$cpf= $_POST['cpf'];
		$setor= $_POST['setor'];
		$curso= $_POST['curso'];

		$CRUD->	Cadastrar_servidor($nome,$email,$cpf,$setor,$curso);	
	}
?>