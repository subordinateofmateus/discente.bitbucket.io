<?php  
	require 'CRUD.class.php';

	$CRUD= new CRUD();
?>
<script type="text/javascript">
		function mascaraCpf(cpf){
			if (cpf.value.length==3){
				cpf.value=cpf.value+".";
			} if (cpf.value.length==7){
				cpf.value=cpf.value+".";
			} if (cpf.value.length==11){
				cpf.value=cpf.value+"-";
			}
		}
	</script>



<form action="" method="POST">
	<input type="text" name="nome" required>
	<input type="email" name="email" required>
	<input type="text" name="cpf" id="cpf" placeholder='xxx.xxx.xxx-xx' onkeypress="mascaraCpf(this)" maxlength="14">
	<input type="text" name="matricula" required>
	<input type="date" name="data_nascimento" required>
	<select name="curso" required>	
		<?php  
			$CRUD->select_cursos();
		?>
	</select>
	<input type="submit" name="btn_cadastrar_discente" value='Cadastrar'>
</form>

<?php  
	if (isset($_POST['btn_cadastrar_discente'])) {
		$nome= $_POST['nome'];
		$email= $_POST['email'];
		$cpf= $_POST['cpf'];
		$matricula= $_POST['matricula'];
		$curso= $_POST['curso'];
		$data_nascimento= $_POST['data_nascimento'];

		$CRUD->	Cadastrar_discente($nome,$email,$cpf,$data_nascimento,$matricula,$curso);	
	}
?>