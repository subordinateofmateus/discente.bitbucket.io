

<?php

    function modal($titulo, $mensagem, $cor, $redirecionamento){
        echo "
            <div class='modal fade' id='conf' tabindex='-1' role='dialog' aria-labelledby='padraoLabel' aria-hidden='true'>
                <div class='modal-dialog' role='document'>
                    <div class='modal-content'>
                        <div class='modal-header text-center' style='background-color:#$cor;'>
                            <h5 class='modal-title' id='padraoLabel' style='color:white;'>$titulo</h5>
                        </div>
                        <div class='modal-body text-center' style='font-family: Verdana, Geneva, Tahoma, sans-serif;'>
                            $mensagem
                        </div>		
                    </div>
                </div>
            </div>
            
            <script> 
                $('#conf').modal('show');
        ";
            if ($redirecionamento!='0'){
                echo "
                    setTimeout(function a(){window.location='$redirecionamento';}, 2800);
                ";    
            }
        echo"        
            </script>        
        ";
    }
?>