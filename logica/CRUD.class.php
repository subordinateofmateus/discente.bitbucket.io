<?php

	require_once 'Conexao.class.php';

	require_once 'enviaremail.php';

	require_once 'modal.php';

	date_default_timezone_set('America/Fortaleza');

	class CRUD{

		public function Cadastrar_discente($email,$cpf,$matricula,$curso,$situacao){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql_matriculados= "SELECT * FROM matriculados WHERE matricula=:matricula AND situacao_sistema=:situacao";

			$connection_matriculados= $conn->prepare($sql_matriculados);
				$connection_matriculados->bindValue(':matricula',$matricula);
				$connection_matriculados->bindValue(':situacao',$situacao);
			$connection_matriculados->execute();

			$vl_matriculados= $connection_matriculados->rowCount();

			if ($vl_matriculados>0) {
				$sql_2= "SELECT * FROM discentes WHERE matricula_fk=:matricula";

				$connection_2= $conn->prepare($sql_2);
					$connection_2->bindValue(':matricula',$matricula);
				$connection_2->execute();

				$vl= $connection_2->rowCount();

				if ($vl>0) {
					modal('Aluno já cadastrado!', 'Dados existentes no banco', '641218', 'cadastrar.php');
				} else {
					$sql_discentes= "SELECT * FROM discentes";

					$connection_discentes= $conn->prepare($sql_discentes);
					$connection_discentes->execute();

					$vl_discentes= $connection_discentes->rowCount();

					$senha= uniqid().$vl_discentes;

					$corpoMensagem = "<meta-charset= 'UTF-8'>
							<img src='https://66.media.tumblr.com/08ec0c476ed62aaf7a65b9445b6c3790/tumblr_pwajvl5QKO1yu5agro1_540.png' style=' height: 40px; width: 150px;'><br>
							<b>Nome:</b> ".$email." <br><b>Assunto:</b> Cadastro no Site Protocolo Discente<br><b>Mensagem:</b> ".$nome.", sua senha é:
							<h3>".$senha."</h3>";


					if (smtpmailer($email, 'kelpgf@gmail.com', 'IFCE MARANGUAPE', 'Cadastro Protocolo Discente', $corpoMensagem)) {

					 	$sql_cadastrar= "INSERT INTO discentes VALUES(null, :email, :senha, :matricula, :curso)";

						$connection_cadastrar= $conn->prepare($sql_cadastrar);
							$connection_cadastrar->bindValue(':nome',$nome);
							$connection_cadastrar->bindValue(':email',$email);
							$connection_cadastrar->bindValue(':senha',$senha);
							$connection_cadastrar->bindValue(':matricula',$matricula);
							$connection_cadastrar->bindValue(':curso',$curso);
						$connection_cadastrar->execute();

						$sql_update= "UPDATE matriculados SET situacao_sistema='Cadastrado', nome=:nome, cpf=:cpf, WHERE matricula=:matricula";

						$connection_update= $conn->prepare($sql_update);
							$connection_update->bindValue(':nome',$nome);
							$connection_update->bindValue(':matricula',$matricula);
							$connection_update->bindValue(':cpf',$cpf);
							$connection_update->bindValue(':data_nascimento',$data_nascimento);
							$connection_update->bindValue(':fone',$fone);
							$connection_update->bindValue(':endereco',$endereco);
							$connection_update->bindValue(':cidade',$cidade);
							$connection_update->bindValue(':estado',$estado);
							$connection_update->bindValue(':cep',$cep);
						$connection_update->execute();

						modal('Cadastro realizado!!!', 'Confira sua senha no Gmail', '1BAC91', 'cadastrar.php');
					}
				}
			} else {
				modal('Dados não registrados!!!', 'Aluno não matriculado', '641218', 'cadastrar.php');
			}
		}

		/*=====================================================================================================================================================================================================*/

		public function select_pedidos_pendentes_servidor($id_setor_servidor,$id_curso_servidor){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT id_pedidos_pendentes, nome_discente, titulo_solicitacao, data_hora FROM pedidos_pendentes INNER JOIN solicitacoes_setor_campus
						ON id_solicitacao_setor_campus_fk=id_solicitacao_setor_campus INNER JOIN solicitacoes ON id_solicitacao_fk=id_solicitacao INNER JOIN discentes ON id_discente_fk=id_discente
						INNER JOIN matriculados ON matricula=matricula_fk INNER JOIN setores_campus ON id_setor_campus=id_setor_campus_fk INNER JOIN campus ON id_campus=matriculados.id_campus_fk
						INNER JOIN instituto ON id_instituto=id_instituto_fk WHERE solicitacoes_setor_campus.id_setor_campus_fk=:id_setor_servidor
						AND discentes.id_curso_campus_fk=:id_curso_servidor AND status=0";

			$connection= $conn->prepare($sql);
				$connection->bindValue(':id_setor_servidor', $id_setor_servidor);
				$connection->bindValue(':id_curso_servidor', $id_curso_servidor);
			$connection->execute();

			$vl= $connection->rowCount();

			if($vl>0){
				$pedidos_pendendes= $connection->fetchAll();
				foreach ($pedidos_pendendes as $key) {
					echo "
					<tr>
						<th scope='row'><a href='solicitacao.php?id=".$key['id_pedidos_pendentes']."'>".$key['id_pedidos_pendentes']."</a></th>
						<td>".$key['nome_discente']."</td>
						<td>".utf8_encode($key['titulo_solicitacao'])."</td>
						<td>".date('d/m/Y H:m:s', strtotime($key['data_hora']))."</td>
					</tr>
					";
				}
				echo "
					</table>
				";
			}
		}

		/*=====================================================================================================================================================================================================*/

		public function select_pedidos_resolvidos_servidor($id,$id_curso_servidor){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT id_pedidos_resolvidos, numero_SEI, pedidos_resolvidos.data_hora, justificativa_servidor, situacao, nome_discente, nome_servidor, titulo_solicitacao,
						id_pedidos_pendente_fk FROM pedidos_resolvidos INNER JOIN pedidos_pendentes ON id_pedidos_pendentes=id_pedidos_pendente_fk INNER JOIN solicitacoes_setor_campus
						ON id_solicitacao_setor_campus_fk=id_solicitacao_setor_campus INNER JOIN solicitacoes ON id_solicitacao_fk=id_solicitacao INNER JOIN discentes ON id_discente_fk=id_discente
						INNER JOIN servidores ON id_servidor_fk=id_servidor INNER JOIN matriculados ON matricula=matricula_fk INNER JOIN setores_campus
						ON id_setor_campus=servidores.id_setor_campus_fk INNER JOIN campus ON id_campus=matriculados.id_campus_fk INNER JOIN instituto ON id_instituto=id_instituto_fk
						WHERE id_servidor=:id AND discentes.id_curso_campus_fk=:id_curso_servidor";

			$connection= $conn->prepare($sql);
				$connection->bindValue(':id', $id);
				$connection->bindValue(':id_curso_servidor', $id_curso_servidor);
			$connection->execute();

			$vl= $connection->rowCount();

			if($vl>0){
				$pedidos_pendendes= $connection->fetchAll();
				foreach ($pedidos_pendendes as $key) {
					echo "
					<tr>
					<th scope='row'><a href='solicitacao.php?id_r=".$key['id_pedidos_resolvidos']."'>".$key['id_pedidos_pendente_fk']."</a></th>
					<td>".$key['nome_discente']."</td>
					<td>".utf8_encode($key['titulo_solicitacao'])."</td>
					<td>".date('d/m/Y H:i:s', strtotime($key['data_hora']))."</td>
					<th>".$key['numero_SEI']."</th>
					<td>".strtoupper($key['situacao'])."</td>
					</tr>
					";
				}

				echo "
					</table>
				";
			}
		}

		/*====================================================================================================================================================================*/

		public function select_pedidos_pendentes(){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT id_pedidos_pendentes, nome_discente, titulo_solicitacao, data_hora, setores.nome_setor FROM pedidos_pendentes INNER JOIN solicitacoes_setor_campus
						ON id_solicitacao_setor_campus_fk=id_solicitacao_setor_campus INNER JOIN solicitacoes ON id_solicitacao_fk=id_solicitacao INNER JOIN discentes
						ON id_discente_fk=id_discente INNER JOIN setores_campus ON id_setor_campus=id_setor_campus_fk INNER JOIN setores ON id_setor=id_setor_fk INNER JOIN campus
						ON id_campus=setores_campus.id_campus_fk INNER JOIN instituto ON id_instituto=id_instituto_fk INNER JOIN matriculados ON matricula=matricula_fk WHERE status=0
						ORDER BY data_hora";

			$connection= $conn->prepare($sql);
			$connection->execute();

			$vl= $connection->rowCount();

			if($vl>0){
				$pedidos_pendendes= $connection->fetchAll();

				foreach ($pedidos_pendendes as $key) {
					echo "
						<tr>
							<th	>".$key['id_pedidos_pendentes']."</td>
							<td>".$key['nome_discente']."</td>
							<td>".utf8_encode($key['titulo_solicitacao'])."</td>
							<td>".date('d/m/Y', strtotime($key['data_hora']))."</td>
							<td>".utf8_encode($key['nome_setor'])	."</td>
						</tr>
					";
				}
			}
		}

		/*====================================================================================================================================================================*/

		public function select_pedidos_resolvidos(){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT id_pedidos_resolvidos, id_pedidos_pendentes, numero_SEI, pedidos_resolvidos.data_hora, justificativa_servidor, situacao, nome_discente, nome_servidor,
						titulo_solicitacao FROM pedidos_resolvidos INNER JOIN pedidos_pendentes ON id_pedidos_pendentes=id_pedidos_pendente_fk INNER JOIN solicitacoes_setor_campus
						ON id_solicitacao_setor_campus_fk=id_solicitacao_setor_campus INNER JOIN solicitacoes ON id_solicitacao_fk=id_solicitacao INNER JOIN setores_campus
						ON id_setor_campus=id_setor_campus_fk INNER JOIN setores ON id_setor=id_setor_fk INNER JOIN campus ON id_campus=setores_campus.id_campus_fk INNER JOIN instituto
						ON id_instituto=id_instituto_fk INNER JOIN discentes ON id_discente_fk=id_discente INNER JOIN servidores ON id_servidor_fk=id_servidor INNER JOIN matriculados
						ON matricula=matricula_fk ORDER BY data_hora";

			$connection= $conn->prepare($sql);
			$connection->execute();

			$vl= $connection->rowCount();

			if($vl>0){
				$pedidos_pendendes= $connection->fetchAll();

				foreach ($pedidos_pendendes as $key) {
					echo "
						<tr>
							<th>".$key['id_pedidos_pendentes']."</td>
							<td>".$key['nome_discente']."</td>
							<td>".utf8_encode($key['titulo_solicitacao'])."</td>
							<td>".date('d/m/Y H:i:s', strtotime($key['data_hora']))."</td>
							<td>".$key['nome_servidor']."</td>
							<th>".$key['numero_SEI']."</th>
							<td class='text-uppercase'>".$key['situacao']."</td>
						</tr>
					";
				}
			}
		}

		/*====================================================================================================================================================================*/

		public function select_setores(){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT id_setor_campus, id_setor, nome_setor FROM setores_campus INNER JOIN setores ON id_setor_fk=id_setor INNER JOIN campus
						ON id_campus_fk=id_campus INNER JOIN instituto ON id_instituto=id_instituto_fk WHERE id_campus_fk=:id_campus AND id_instituto=:id_instituto ORDER BY nome_setor ASC";

			$connection= $conn->prepare($sql);
				$connection->bindValue(":id_campus", $_SESSION['id_campus']);
				$connection->bindValue(":id_instituto", $_SESSION['id_instituto']);
			$connection->execute();

			$vl= $connection->rowCount();

			if($vl>0){
				$setores= $connection->fetchAll();

				foreach ($setores as $key) {
					if (isset($_SESSION['id_setor_campus']) && $_SESSION['id_setor_campus']==$key['id_setor_campus']){
						echo "
							<option value='".$key['id_setor_campus']."' selected>".utf8_encode($key['nome_setor'])."</option>
						";
					} else {
						echo "
							<option value='".$key['id_setor_campus']."'>".utf8_encode($key['nome_setor'])."</option>
						";
					}
				}
			}
		}

		/*====================================================================================================================================================================*/

		public function select_cursos(){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT id_curso_campus, id_curso, nome_curso, turno FROM cursos_campus INNER JOIN cursos ON id_curso=id_curso_fk INNER JOIN campus
						ON id_campus_fk=id_campus INNER JOIN instituto ON id_instituto=id_instituto_fk WHERE id_campus_fk=:id_campus AND id_instituto=:id_instituto ORDER BY nome_curso ASC";

			$connection= $conn->prepare($sql);
				$connection->bindValue(":id_campus", $_SESSION['id_campus']);
				$connection->bindValue(":id_instituto", $_SESSION['id_instituto']);
			$connection->execute();

			$vl= $connection->rowCount();

			if($vl>0){
				$cursos= $connection->fetchAll();

				foreach ($cursos as $key) {
					if (isset($_SESSION['id_curso_campus']) && $_SESSION['id_curso_campus']==$key['id_curso_campus']){
						echo "
							<option value='".$key['id_curso_campus']."' selected>".utf8_encode($key['nome_curso'])."</option>
						";
					} else {
						echo "
							<option value='".$key['id_curso_campus']."'>".utf8_encode($key['nome_curso'])."</option>
						";
					}
				}
			}
		}

		/*====================================================================================================================================================================*/

		public function list_cursos(){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT id_curso_campus, id_curso, nome_curso, turno FROM cursos_campus INNER JOIN cursos ON id_curso=id_curso_fk INNER JOIN campus
						ON id_campus_fk=id_campus INNER JOIN instituto ON id_instituto=id_instituto_fk WHERE id_campus_fk=:id_campus AND id_instituto=:id_instituto AND id_curso!=0";

			$connection= $conn->prepare($sql);
				$connection->bindValue(":id_campus", $_SESSION['id_campus']);
				$connection->bindValue(":id_instituto", $_SESSION['id_instituto']);
			$connection->execute();

			$vl= $connection->rowCount();

			if($vl>0){
				$cursos= $connection->fetchAll();

				foreach ($cursos as $key) {
					echo "	<tr>
					<td>".$key['nome_curso']."</td>
					<td><button type='submit' class='btn btn-danger btn-sm' value='".$key['id_curso_campus']." - ' ".$key['turno']." name='btn_excluir_curso'><i class='fas fa-times'></i></button></td>
					</tr>
					";
				}
			}
		}

		/*====================================================================================================================================================================

		public function deletar_servidor($id_servidor){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql="DELETE FROM servidores WHERE id_servidor=:id_servidor";
			$connection = $conn->prepare($sql);
			$connection->bindValue(':id_servidor', $id_servidor);
			$connection->execute();

			echo "
				<script>
					window.location='adm.php';
				</script>
			";
		}

		====================================================================================================================================================================

		public function deletar_cursos($id_curso){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql="DELETE FROM cursos WHERE id_curso=:id_curso";

			$connection= $conn->prepare($sql);
				$connection->bindValue(':id_curso',$id_curso);
			$connection->execute();

			echo "
				<script>
					window.location='adm.php';
				</script>
			";
		}

		====================================================================================================================================================================*/

		public function list_servidor(){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT id_servidor, nome_servidor FROM servidores INNER JOIN cursos_campus ON id_curso_campus=id_curso_campus_fk INNER JOIN cursos ON id_curso=id_curso_fk INNER JOIN campus
						ON id_campus_fk=id_campus INNER JOIN instituto ON id_instituto=id_instituto_fk WHERE id_campus_fk=:id_campus AND id_instituto=:id_instituto ORDER BY nome_servidor ASC";

			$connection= $conn->prepare($sql);
				$connection->bindValue(":id_campus", $_SESSION['id_campus']);
				$connection->bindValue(":id_instituto", $_SESSION['id_instituto']);
			$connection->execute();

			$vl= $connection->rowCount();

			if($vl>0){
				$cursos= $connection->fetchAll();

				foreach ($cursos as $key) {
					echo "
					<tr>
					<td>".$key['nome_servidor']."</td>
					<td><a class='btn btn-warning btn-sm' href='editar_serv.php?id=".$key['id_servidor']."'><i class='fas fa-edit'></i></a></td>
					<td><button type='submit' class='btn btn-danger btn-sm' value='".$key['id_servidor']."' name='btn_excluir_curso'><i class='fas fa-times'></i></button></td>
					</tr>";
				}
			}
		}

		/*====================================================================================================================================================================*/

		public function cadastrar_servidor($nome,$email,$cpf,$setor_campus,$curso_campus){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql_servidores= "SELECT * FROM servidores WHERE nome_servidor=:nome AND cpf=:cpf AND email_servidor=:email AND id_setor_campus_fk=:setor AND id_curso_campus_fk=:curso";

			$connection_servidores= $conn->prepare($sql_servidores);
				$connection_servidores->bindValue(':nome',$nome);
				$connection_servidores->bindValue(':cpf',$cpf);
				$connection_servidores->bindValue(':email',$email);
				$connection_servidores->bindValue(':setor',$setor_campus);
				$connection_servidores->bindValue(':curso',$curso_campus);
			$connection_servidores->execute();

			$vl_servidores= $connection_servidores->rowCount();

			if ($vl_servidores==0) {
				$sql_quantidade_servidores= "SELECT * FROM servidores";

				$connection_quantidade_servidores= $conn->prepare($sql_quantidade_servidores);
				$connection_quantidade_servidores->execute();

				$vl_quantidade_servidores= $connection_quantidade_servidores->rowCount();

				$id_novo= '32'.mt_rand(0000, 9999).$vl_quantidade_servidores;

				$senha= uniqid().$vl_quantidade_servidores;

				$corpoMensagem = "<meta-charset= 'UTF-8'>
						<img src='https://66.media.tumblr.com/08ec0c476ed62aaf7a65b9445b6c3790/tumblr_pwajvl5QKO1yu5agro1_540.png' style=' height: 40px; width: 150px;'><br>
						<b>Nome:</b> ".$email." <br><b>Assunto:</b> Cadastro no Site Protocolo Discente<br><b>Mensagem:</b> ".$nome.", <br> Seu id: ".$id_novo.". <br> Sua senha é:
						".$senha."";

				if (smtpmailer($email, 'kelpgf@gmail.com', 'IFCE MARANGUAPE', 'Cadastro Protocolo Discente', $corpoMensagem)) {

					$sql_cadastrar= "INSERT INTO servidores VALUES(:id_servidor, :senha, :nome_servidor, :cpf, email, :setor, :curso)";

					$connection_cadastrar= $conn->prepare($sql_cadastrar);
						$connection_cadastrar->bindValue(':id_servidor',$id_novo);
						$connection_cadastrar->bindValue(':senha',$senha);
						$connection_cadastrar->bindValue(':nome_servidor',$nome);
						$connection_cadastrar->bindValue(':cpf',$cpf);
						$connection_cadastrar->bindValue(':email',$email);
						$connection_cadastrar->bindValue(':setor',$setor_campus);
						$connection_cadastrar->bindValue(':curso',$curso_campus);
					$connection_cadastrar->execute();

					modal('Cadastro realizado!!!', 'Mensagem enviada ao Gmail cadastrado', '1BAC91', 'cadastrar.php');
				}
			} else {
				modal('Dados existentes', 'Esse servidor(a) já está cadastrado(a)','641218','adm.php');
			}
		}

		/*====================================================================================================================================================================*/

		public function alterar_senha($id, $senha, $opc){
			$con= new Conexao();
			$conn= $con->Connection();

			if ($opc==1) {
				$table= 'discentes';
				$id_consulta= 'id_discente';
			} if ($opc==2) {
				$table= 'servidores';
				$id_consulta= 'id_servidor';
			}

			$corpoMensagem = "<meta-charset= 'UTF-8'>
							<img src='https://66.media.tumblr.com/08ec0c476ed62aaf7a65b9445b6c3790/tumblr_pwajvl5QKO1yu5agro1_540.png' style=' height: 40px; width: 150px;'><br>
							<b>Nome:</b> ".$_SESSION['email']." <br><b>Assunto:</b> Protocolo Discente - Alteração de Senha: <br><b>Mensagem:</b> ".$_SESSION['nome'].", sua senha nova é:
							<h3>".$senha."</h3>
						";

			if (smtpmailer($_SESSION['email'], 'kelpgf@gmail.com', 'IFCE MARANGUAPE', 'Alteração de Senha - Site Protocolo Discente', $corpoMensagem)) {
				$sql= "UPDATE $table SET senha=:senha WHERE $id_consulta=:id";

				$connection= $conn->prepare($sql);
					$connection->bindValue(':id',$id);
					$connection->bindValue(':senha',$senha);
				$connection->execute();

				$_SESSION['senha']=$senha;

				modal('Senha alterada com sucesso!', 'Verifique seu Gmail para confirmá-la', '1BAC91', '0');
			}
		}

		/*====================================================================================================================================================================*/

		public function responder_pedido($id_pedidos_pendentes, $id, $SEI, $situacao, $justificativa_servidor){
			$con= new Conexao();
			$conn= $con->Connection();

			$data_hora= date('Y-m-d H:i:s');

			$sql= "SELECT nome_discente, matricula, nome_curso, turno, email, titulo_solicitacao FROM pedidos_pendentes INNER JOIN discentes ON id_discente_fk=id_discente
						INNER JOIN matriculados ON matricula=matricula_fk INNER JOIN solicitacoes_setor_campus ON id_solicitacao_setor_campus=id_solicitacao_setor_campus_fk INNER JOIN solicitacoes
						ON id_solicitacao_fk=id_solicitacao INNER JOIN cursos_campus ON id_curso_campus_fk=id_curso_campus INNER JOIN cursos ON id_curso=id_curso_fk
						WHERE id_pedidos_pendentes=:id_pedidos_pendentes AND status=0";

			$connection= $conn->prepare($sql);
				$connection->bindValue(':id_pedidos_pendentes',$id_pedidos_pendentes);
			$connection->execute();

			$vl= $connection->rowCount();

			if ($vl>0) {
				$dados= $connection->fetchAll();

				$corpoMensagem = "
					<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'>

					<div style='border-radius: 5px; background-color: white; margin-left: 25%; margin-right:25%;margin-top: 10px; font-family: verdana;'>
						<center>
							<img src='https://66.media.tumblr.com/08ec0c476ed62aaf7a65b9445b6c3790/tumblr_pwajvl5QKO1yu5agro1_540.png' style='height: 50px; width: 175px; margin-top: 12px;'>
							<br>
							<h2 style='font-family: courier; margin-top: 10px;'>".$dados[0]['titulo_solicitacao']."</h2>
						</center>
						<div>
							<b>Nome: </b>".$dados[0]['nome_discente']." <br>
							<b>Matrícula: </b> ".$dados[0]['matricula']." <br>
							<b>Curso: </b> ".$dados[0]['nome_curso']."<br>
							<b>Turno: </b> ".utf8_encode($dados[0]['turno'])."<br>
							<b>JUSTIFICATIVA: </b>$justificativa_servidor <br>
							<b>RESPOSTA: </b> ".strtoupper($situacao)."<br>
						</div>
						<br>
					</div>
				";

				if (smtpmailer($dados[0]['email'], 'kelpgf@gmail.com', 'IFCE MARANGUAPE', 'Resposta de solicitação - Protocolo Discente', $corpoMensagem)) {

					$sql= "INSERT INTO pedidos_resolvidos VALUES (null, :id_pedidos_pendentes, :id, :SEI, :data_hora, :justificativa_servidor, :situacao)";

					$connection= $conn->prepare($sql);
						$connection->bindValue(':id_pedidos_pendentes',$id_pedidos_pendentes);
						$connection->bindValue(':id',$id);
						$connection->bindValue(':SEI',$SEI);
						$connection->bindValue(':data_hora',$data_hora);
						$connection->bindValue(':justificativa_servidor',$justificativa_servidor);
						$connection->bindValue(':situacao',$situacao);
					$connection->execute();


					$sql_pedido= "UPDATE pedidos_pendentes SET status=1 WHERE id_pedidos_pendentes=:id_pedidos_pendente";

					$connection_pedido= $conn->prepare($sql_pedido);
						$connection_pedido->bindValue(':id_pedidos_pendente',$id_pedidos_pendentes);
					$connection_pedido->execute();

					modal('Solicitação concluída!', 'Resposta enviada ao discente', '1BAC91','servidor.php');
				}
			} else {
				modal('ERRO!!!', 'Pedido inexistente', '641218','servidor.php');
			}
		}

		/*====================================================================================================================================================================*/

		public function mostrar_dados_pedido($id_pedidos_pendentes){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT justificativa, nome_tipo, data_hora, materia, data_falta, titulo_solicitacao, nome_discente, nome_curso FROM `pedidos_pendentes` INNER JOIN solicitacoes_setor_campus
						ON id_solicitacao_setor_campus=id_solicitacao_setor_campus_fk INNER JOIN solicitacoes ON id_solicitacao_fk=id_solicitacao INNER JOIN discentes ON id_discente=id_discente_fk
						INNER JOIN matriculados ON matricula=matricula_fk INNER JOIN cursos_campus ON id_curso_campus_fk=id_curso_campus INNER JOIN cursos ON id_curso=id_curso_fk
						INNER JOIN arquivos_pedidos ON id_pedido_pendente_fk=id_pedidos_pendentes WHERE id_pedidos_pendentes=:id_pedidos_pendentes ORDER BY data_hora ASC";

			$connection= $conn->prepare($sql);
				$connection->bindValue(':id_pedidos_pendentes',$id_pedidos_pendentes);
			$connection->execute();

			$vl= $connection->rowCount();

			if ($vl>0) {
				$dados_pedido= $connection->fetchAll();

				foreach ($dados_pedido as $key) {
					$extencao = substr($key['nome_tipo'], -3);

					echo "
						<div class='card-header bg-gray-200 text-dark'>
						<h5 class='mt-2'>
						<strong>N° ".$id_pedidos_pendentes."</strong> - ".utf8_encode($key['titulo_solicitacao'])." - ".
						date('d/m/Y', strtotime($key['data_hora']))."
						</h5>
						</div>

						<div class='card-body text-dark' style='font-size: 20px;'>
						<center>
						<label><strong>Nome</strong></label>
						<input type='text' class='form-control col-lg-3 mb-3 bg-white' disabled value='".$key['nome_discente']."'>
						<label><strong>Curso</strong></label>
						<input type='text' class='form-control col-lg-3 mb-3 bg-white' disabled value='". utf8_encode( $key['nome_curso'])."'>
					";

					if ($key['materia']!='nenhuma') {
						echo "
						<label><strong>Disciplina</strong></label>
						<input type='text' class='form-control col-lg-3 mb-3 bg-white' disabled value='".$key['materia']."'>
						";
					}
					if ($key['data_falta']!='0000-00-00') {
						echo "
						<label><strong>Data falta</strong></label>
						<input type='date' class='form-control col-lg-3 mb-3 bg-white' disabled
						value='".$key['data_falta']."'>
						";
					}

					echo "
					<label><strong>Justificativa</strong></label>
					<textarea class='form-control col-lg-3 text-justify bg-white' disabled style='height: 140px;'>".$key['justificativa']."
					</textarea>
					";

					if ($extencao=='pdf') {
						echo "
						<a class='btn btn-secondary mt-3' href='../logica/arquivos/".$key['nome_tipo']."' target='_blank'>Abrir Comprovante</a><br>
						";
					} else {
						echo "<button class='btn btn-secondary mt-3' data-toggle='modal' data-target='#exampleModal'>
								Abrir comprovante
							  </button><br>
						<!-- Modal -->
						<div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
						  <div class='modal-dialog' role='document'>
						    <div class='modal-content'>
						      <div class='modal-header'>
						        <h5 class='modal-title' id='exampleModalLabel'>Comprovante</h5>
						        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						          <span aria-hidden='true'>&times;</span>
						        </button>
						      </div>
						      <div class='modal-body'>
						       <img src='../logica/arquivos/".$key['nome_tipo']."' width='100%'>
						      </div>
						    </div>
						  </div>
						</div>

						";
					}
				}
			echo "
			<div class='btn-group mt-3' role='group' aria-label='Basic example'>
				 <button type='button' class='btn btn-success'  data-toggle='modal' data-target='#aceitar'>Aceitar</button>
				 <button type='button' class='btn btn-danger'  data-toggle='modal' data-target='#recusar'>Recusar</button>
			 </div>";
			} else { modal('ERRO!', 'Pedido inexistente', '641218', '0');}
		}

		/*====================================================================================================================================================================*/

		public function mostrar_dados_pedido_resolvido($id_pedidos_resolvido){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT id_pedidos_pendentes, justificativa, nome_tipo, pedidos_resolvidos.data_hora, materia, data_falta, titulo_solicitacao, nome_discente, nome_curso, numero_SEI,
						nome_servidor,justificativa_servidor, situacao FROM `pedidos_resolvidos` INNER JOIN pedidos_pendentes ON id_pedidos_pendentes=id_pedidos_pendente_fk INNER JOIN servidores
						ON id_servidor=id_servidor_fk INNER JOIN solicitacoes_setor_campus ON id_solicitacao_setor_campus=id_solicitacao_setor_campus_fk INNER JOIN solicitacoes
						ON id_solicitacao_fk=id_solicitacao INNER JOIN discentes ON id_discente=id_discente_fk INNER JOIN matriculados ON matricula=matricula_fk INNER JOIN cursos_campus
						ON servidores.id_curso_campus_fk=id_curso_campus INNER JOIN cursos ON id_curso=id_curso_fk INNER JOIN arquivos_pedidos ON id_pedido_pendente_fk=id_pedidos_pendentes
						WHERE id_pedidos_resolvidos=:id_pedidos_resolvido ORDER BY data_hora ASC";

			$connection= $conn->prepare($sql);
				$connection->bindValue(':id_pedidos_resolvido',$id_pedidos_resolvido);
			$connection->execute();

			$vl= $connection->rowCount();

			if ($vl>0) {
				$dados_pedido= $connection->fetchAll();

				foreach ($dados_pedido as $key) {
					$extencao = substr($key['nome_tipo'], -3);

					echo "
						<div class='card-header bg-gray-200 text-dark'>
						<h5 class='mt-2'>
						<strong>N° ".$key['id_pedidos_pendentes']."</strong> - ".utf8_encode($key['titulo_solicitacao'])." - ".
						date('d/m/Y', strtotime($key['data_hora']))."
						</h5>
						</div>

						<div class='card-body text-dark' style='font-size: 20px;'>
						<center>
						<label><strong>Nome</strong></label>
						<input type='text' class='form-control col-lg-3 mb-3 bg-white' disabled value='".$key['nome_discente']."'>
						<label><strong>Curso</strong></label>
						<input type='text' class='form-control col-lg-3 mb-3 bg-white' disabled value='". utf8_encode( $key['nome_curso'])."'>
					";

					if ($key['materia']!='nenhuma') {
						echo "
						<label><strong>Disciplina</strong></label>
						<input type='text' class='form-control col-lg-3 mb-3 bg-white' disabled value='".$key['materia']."'>
						";
					}
					if ($key['data_falta']!='0000-00-00') {
						echo "
						<label><strong>Data falta</strong></label>
						<input type='date' class='form-control col-lg-3 mb-3 bg-white' disabled
						value='".$key['data_falta']."'>
						";
					}

					echo "
					<div class='form-group'>
					<label><strong>Justificativa do aluno</strong></label>
					<textarea class='form-control col-lg-3 text-justify bg-white' disabled style='height: 140px;'>".$key['justificativa']."
					</textarea>
					</div>
					<hr class='col-3'>";

					if ($key['situacao'] == 'recusado') {
						echo	"<div class='alert alert-danger col-3 text-uppercase' role='alert'>" . $key['situacao'] . "</div>
						<div class='form-group'>
						<label><strong>Justificativa do servidor</strong></label>
						<input type='text' class='form-control col-lg-3 mb-3 bg-white' disabled value='".$key['justificativa_servidor']."'>
						</div>";
					} else {
						echo	"<div class='alert alert-success col-3 text-uppercase' role='alert'>" . $key['situacao'] . "</div>";
					}
				}
			} else {
				modal('ERRO!', 'Pedido inexistente', '641218', '0');
			}
		}
		/*====================================================================================================================================================================*/

		public function mostrar_pedidos_pendentes_alunos($id){
			$con= new Conexao();
			$conn= $con->Connection();

			try{
				$sql= "SELECT id_pedidos_pendentes, data_hora, titulo_solicitacao FROM pedidos_pendentes INNER JOIN discentes on id_discente_fk=id_discente INNER JOIN matriculados
							ON matricula=matricula_fk INNER JOIN solicitacoes_setor_campus ON id_solicitacao_setor_campus=id_solicitacao_setor_campus_fk INNER JOIN solicitacoes
							ON id_solicitacao=id_solicitacao_fk WHERE status=0 AND id_discente_fk=:id_discente ORDER BY data_hora DESC LIMIT 5";

				$connectionn= $conn->prepare($sql);
				$connectionn->bindValue(':id_discente',$id);
				$connectionn->execute();

				$vl= $connectionn->rowCount();

				if ($vl>0) {
					$dados= $connectionn->fetchAll();

					for ($i=0; $i < $vl; $i++) {
						echo "
						<div class='alert alert-info'>
						<strong>Nº ".$dados[$i]['id_pedidos_pendentes']." </strong> &nbsp; ".utf8_encode($dados[$i]['titulo_solicitacao'])."
						- ".date('d/m/Y H:i:s', strtotime($dados[$i]['data_hora']))."
						</div>

						";
					}
				} else {
					echo "
					<div class='alert'>
					<strong>Nenhuma solicitação pendente</strong>
					</div>
					";
				}
			} catch(Exception $e) {
				modal('ERRO', 'ERRO NO SISTEMA', '641218', '0');
			}
		}

		/*====================================================================================================================================================================*/

		public function mostrar_pedidos_resolvidos_alunos($id){
			$con= new Conexao();
			$conn= $con->Connection();

			try{
				$sql= "SELECT id_pedidos_resolvidos, id_pedidos_pendentes, situacao, pedidos_resolvidos.data_hora, numero_SEI, titulo_solicitacao FROM pedidos_resolvidos
							INNER JOIN pedidos_pendentes ON id_pedidos_pendente_fk=id_pedidos_pendentes INNER JOIN discentes ON id_discente_fk=id_discente INNER JOIN matriculados
							ON matricula=matricula_fk INNER JOIN solicitacoes_setor_campus ON id_solicitacao_setor_campus=id_solicitacao_setor_campus_fk INNER JOIN solicitacoes
							ON id_solicitacao=id_solicitacao_fk WHERE status=1 AND id_discente_fk=:id_discente ORDER BY data_hora DESC LIMIT 5";

				$connectionn= $conn->prepare($sql);
				$connectionn->bindValue(':id_discente',$id);
				$connectionn->execute();

				$vl= $connectionn->rowCount();

				if ($vl>0) {
					$dados= $connectionn->fetchAll();

					for ($i=0; $i < $vl; $i++) {
						if	($dados[$i]['situacao']=='recusado'){
							$class= 'danger';
						}
						if	($dados[$i]['situacao']=='aceito'){
							$class= 'success';
						}
						echo "
							<div class='alert alert-$class' id='conc'>
								<strong>Nº ".$dados[$i]['id_pedidos_pendentes']." </strong> &nbsp; ".utf8_encode($dados[$i]['titulo_solicitacao'])."
								- ".date('d/m/Y H:i:s', strtotime($dados[$i]['data_hora']))."
							</div>
						";
					}
 				}
			} catch(Exception $e) {
				modal('ERRO', 'ERRO NO SISTEMA', '641218', '0');
			}
		}

		/*====================================================================================================================================================================*/

		public function cadastro_curso($nome_curso,$turno_curso){
			$con = new Conexao();
			$conn = $con->Connection();

			$sql = "INSERT INTO cursos VALUES(null,:nome_curso)";

			$connection = $conn -> prepare($sql);
				$connection -> bindValue(":nome_curso",$nome_curso);
			$connection -> execute();

			$id_curso= $conn->lastInsertId();


			$sql_1 = "INSERT INTO cursos_campus VALUES(null,:id_campus,:id_curso,:turno)";

			$connection_1 = $conn -> prepare($sql_1);
				$connection_1 -> bindValue(":id_campus",$_SESSION['id_campus']);
				$connection_1 -> bindValue(":id_curso",$id_curso);
				$connection_1 -> bindValue(":turno",$turno_curso);
			$connection_1 -> execute();
		}

		/*====================================================================================================================================================================*/

		public function mostrar_alunos_campos(){
			$con= new Conexao();
			$conn= $con->Connection();

			try{
				$sql= "SELECT * FROM matriculados WHERE id_campus_fk=:id_campus ORDER BY nome_discente ASC";

				$connection= $conn -> prepare($sql);
					$connection->bindValue(":id_campus", $_SESSION['id_campus']);
				$connection -> execute();

				$vl= $connection->rowCount();

				if ($vl>0) {
					$dados= $connection->fetchAll();

					$endereco=utf8_decode('endereco');

					for ($i=0; $i < $vl; $i++) {
						echo "<tr>
								<td>".$dados[$i]['matricula']."</td>
								<td>".$dados[$i]['nome_discente']."</td>
								<td>".$dados[$i]['rg']."</td>
								<td>".$dados[$i]['cpf']."</td>
								<td>".date("d/m/Y", strtotime($dados[$i]['data_nascimento']))."</td>
								<td>".$dados[$i]['fone']."</td>
								<td>".$dados[$i][$endereco]."</td>
								<td>".$dados[$i]['CEP']."</td>
								<td>".$dados[$i]['cidade']."</td>
								<td>".$dados[$i]['estado']."</td>
							</tr>
						";
					}
				}
			} catch(Exception $e) {
				modal('ERRO', 'ERRO NO SISTEMA ', '641218', '0');
			}

		}

		/*====================================================================================================================================================================*/

		public function mostrar_alunos_cadastrados(){
			$con= new Conexao();
			$conn= $con->Connection();

			try{
				$sql= "SELECT * FROM discentes INNER JOIN matriculados ON matricula=matricula_fk INNER JOIN cursos_campus ON id_curso_campus=id_curso_campus_fk INNER JOIN cursos
							ON id_curso=id_curso_fk WHERE cursos_campus.id_campus_fk=:id_campus AND situacao_sistema=:situacao ORDER BY nome_discente";

				$connection= $conn -> prepare($sql);
					$connection -> bindValue(":situacao","Cadastrado");
						$connection->bindValue(":id_campus", $_SESSION['id_campus']);
				$connection -> execute();

				$vl= $connection->rowCount();

				if ($vl>0) {
					$dados= $connection->fetchAll();

					for ($i=0; $i < $vl; $i++) {
						echo "<tr>
								<th><a href='editar_alu.php?matricula=".$dados[$i]['matricula']."'>".$dados[$i]['matricula_fk']."</a></td>
								<td>".$dados[$i]['nome_discente']."</td>
								<td>".$dados[$i]['email']."</td>
								<td>".$dados[$i]['nome_curso']."</td>
							</tr>
						";
					}
				}
			}catch(Exception $e){
				modal('ERRO', 'ERRO NO SISTEMA', '641218', '0');
			}
		}

		/*====================================================================================================================================================================*/

		public function dados_servidor($id){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT * FROM servidores INNER JOIN cursos_campus ON id_curso_campus=id_curso_campus_fk INNER JOIN cursos ON id_curso=id_curso_fk INNER JOIN setores_campus
						ON id_setor_campus=id_setor_campus_fk INNER JOIN setores ON id_setor=id_setor_fk WHERE id_servidor=:id";

			$connection= $conn->prepare($sql);
				$connection->bindValue(':id',$id);
			$connection->execute();

			$vl= $connection->rowCount();

			if ($vl>0){
				$dados= $connection->fetchAll();

				echo "
				<div class='form-group'>
					<label for='exampleInputEmail1'>Nome</label>
					<input required type='text' class='form-control' value='".$dados[0]['nome_servidor']."' name='nome'>
				</div>
				<div class='form-group'>
					<label for='exampleInputEmail1'>CPF</label>
					<input required type='text' disabled class='form-control' id='cpf'  value='".$dados[0]['cpf']."' placeholder='xxx.xxx.xxx-xx' onkeypress='mascaraCpf(this)' maxlength='14' name='cpf'>
				</div>
				<div class='form-group'>
					<label for='exampleInputEmail1'>Email</label>
					<input required type='email' class='form-control'  value='".$dados[0]['email_servidor']."' name='email'>
				</div>

				";
			}
		}

		/*====================================================================================================================================================================*/

		public function editar_servidor($id_servidor, $nome, $email, $id_setor_campus, $id_curso_campus){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql_update= "UPDATE servidores SET nome_servidor=:nome, email_servidor=:email, id_setor_fk=:id_setor, id_curso_fk=:id_curso WHERE id_servidor=:id_servidor";

			$connection_update= $conn->prepare($sql_update);
				$connection_update->bindValue(':id_servidor',$id_servidor);
				$connection_update->bindValue(':nome',$nome);
				$connection_update->bindValue(':email',$email);
				$connection_update->bindValue(':id_setor',$id_setor_campus);
				$connection_update->bindValue(':id_curso',$id_curso_campus);

			if ($connection_update->execute()) {
				$_SESSION['nome'] = $nome;
				$_SESSION['email'] = $email;
				$_SESSION['id_setor_campus'] = $id_setor_campus;
				$_SESSION['id_curso_campus'] = $id_curso_campus;

				modal('Dados alterados com sucesso','Mudanças registradas','1BAC91','adm.php');
			} else {
				modal('ERRO','Não foi possivel salvar as mudanças','641218','0');
			}
		}

			/*====================================================================================================================================================================*/

			public function esqueci_senha($primeiro,$cpf,$logado){
				$con= new Conexao();
				$conn= $con->Connection();

				if ($logado==1){
					$sql= "SELECT * FROM discentes INNER JOIN matriculados ON matricula=matricula_fk WHERE matricula=:primeiro AND cpf=:cpf;";
				} else {
					$sql= "SELECT * FROM servidores WHERE id=:primeiro AND cpf=:cpf;";
				}

				$connection= $conn->prepare($sql);
					$connection->bindValue(":primeiro", $primeiro);
					$connection->bindValue(":cpf", $cpf);
				$connection->execute();

				$vl= $connection->rowCount();

				if($vl>0){
					$user= $connection->fetchAll();

					if ($logado==1){
						$nome= $user[0]['nome_discente'];
					} else {
						$nome= $user[0]['nome'];
					}

					$corpoMensagem = "
							<img src='https://66.media.tumblr.com/08ec0c476ed62aaf7a65b9445b6c3790/tumblr_pwajvl5QKO1yu5agro1_540.png'><br>
							<b>Nome:</b> ".$user[0]['email']." <br><b>Assunto:</b> Recuperação de Senha <br><b>Mensagem:</b> ".$nome.", sua senha é:
							<h3>".$user[0]['senha']."</h3>";

					if (smtpmailer($user[0]['email'], 'kelpgf@gmail.com', 'IFCE MARANGUAPE', 'Recuperação de Senha', $corpoMensagem)) {
						modal('Senha enviada!', 'Confira sua senha no Gmail', '1BAC91', 'index.html');
					}
				} else {
					modal('Dados incorretos', 'Tente novamente!', '641218', '0');
				}
			}

			/*====================================================================================================================================================================*/

			public function select_aluno_especifico($matricula){
				$con= new Conexao();
				$conn= $con->Connection();

				$sql= "SELECT * FROM matriculados WHERE matricula= :matricula WHERE id_campus_fk=:id_campus";

				$connection= $conn->prepare($sql);
					$connection->bindValue("matricula", $matricula);
					$connection->bindValue("id_campus_fk", $_SESSION['id_campus']);
				$connection->execute();

				$vl= $connection->rowCount();

				if($vl>0){
					$aluno= $connection->fetchAll();

					echo "
							 <form action='' method='POST' style='padding:30px;'>
								<div class='form-row'>
									<div class='form-group col-md-4'>
										<label>Matrícula</label>
										<input type='text' value='".$aluno[0]['matricula']."' name='matricula' class='form-control' disabled>
									</div>
									<div class='form-group col-md-8'>
										<label>Nome</label>
										<input type='text' value='".$aluno[0]['nome_discente']."' name='nome' class='form-control'>
									</div>
								</div>
								<div class='form-row'>
									<div class='form-group col-md-4'>
										<label>CPF</label>
											<input type='text' value='".$aluno[0]['cpf']."' name='cpf' id='cpf' placeholder='xxx.xxx.xxx-xx' onkeypress='mascaraCpf(this)' maxlength='14' class='form-control'>
									</div>
									   <div class='form-group col-md-4'>
										<label>Data de Nascimento</label>
										<input type='date' value='".$aluno[0]['data_nascimento']."' name='data_nascimento' id='' class='form-control'>
									</div>
									<div class='form-group col-md-4'>
										<label>Telefone</label>
										<input type='text' id='fone' value='".$aluno[0]['fone']."' name='fone' onKeyPress='mascaraFone(this)' maxlength='16' placeholder='(xx) x xxxx-xxxx' class='form-control'>
									</div>
								</div>
								<div class='form-group'>
									<label for='inputAddress'>Endereço</label>
									<input type='text' value='".$aluno[0]['endereco']."' name='endereco' class='form-control' id='inputAddress'>
								</div>
								<div class='form-row'>
									<div class='form-group col-md-4'>
										<label>Cidade</label>
										<input type='text' value='".$aluno[0]['cidade']."' name='cidade' class='form-control'>
									</div>
									<div class='form-group col-md-4'>
										<label>Estado</label>
										<input type='text' value='".$aluno[0]['estado']."' name='estado' class='form-control'>
									</div>
									<div class='form-group col-md-4'>
										<label>CEP</label>
										<input type='text' id='cep' value='".$aluno[0]['CEP']."' name='cep' onKeyPress='mascaraCEP(this)' maxlength='9' placeholder='xxxxx-xxx' class='form-control'>
									</div>
								</div> FALTA O RG E O PERIODO DE INSERÇÃO
								<button type='submit' name='btn_atualizar_discente' class='btn btn-primary'>Atualizar</button>
							</form>
					";
				}
			}

			/*====================================================================================================================================================================*/

			public function atualizar_aluno_especifico($matricula, $nome, $rg, $cpf, $data_nascimento, $estado, $cep, $cidade, $endereco, $fone){
				$con= new Conexao();
				$conn= $con->Connection();

				$sql_update= "UPDATE matriculados SET nome=:nome, cpf=:cpf, data_nascimento=:data_nascimento, rg=:rg fone=:fone, endereco=:endereco, cidade=:cidade, estado=:estado, CEP=:CEP WHERE matricula=:matricula";

				$connection_update= $conn->prepare($sql_update);
					$connection_update->bindValue(':nome',$nome);
					$connection_update->bindValue(':rg',$rg);
					$connection_update->bindValue(':cpf',$cpf);
					$connection_update->bindValue(':data_nascimento',$data_nascimento);
					$connection_update->bindValue(':fone',$fone);
					$connection_update->bindValue(':endereco',$endereco);
					$connection_update->bindValue(':cidade',$cidade);
					$connection_update->bindValue(':estado',$estado);
					$connection_update->bindValue(':CEP',$cep);
					$connection_update->bindValue(':matricula',$matricula);
				$connection_update->execute();
			}

			/*====================================================================================================================================================================*/

			public function cadastrar_aluno($matricula, $nome, $semestre_insercao, $rg, $cpf, $data_nascimento, $fone, $endereco, $cidade, $estado, $cep){
				$con= new Conexao();
				$conn= $con->Connection();

				$sql= "SELECT * FROM matriculados WHERE matricula=:matricula";

				$connection= $conn -> prepare($sql);
					$connection -> bindValue(":matricula",$matricula);
				$connection -> execute();

				$vl= $connection->rowCount();

				if ($vl==0) {
					$sql_insert= "INSERT INTO matriculados VALUES(:matricula,:id_campus,:nome,:semestre_insercao,:rg,:cpf,:data_nascimento,:fone,:endereco,:cidade,:estado,:cep,:situacao)";

					$connection_insert= $conn -> prepare($sql_insert);
						$connection_insert -> bindValue(":matricula",$matricula);
						$connection_insert -> bindValue(":nome",$nome);
						$connection_insert -> bindValue(":semestre_insercao",$semestre_insercao);
						$connection_insert -> bindValue(":rg",$rg);
						$connection_insert -> bindValue(":cpf",$cpf);
						$connection_insert -> bindValue(":data_nascimento",$data_nascimento);
						$connection_insert -> bindValue(":fone",$fone);
						$connection_insert -> bindValue(":endereco",$endereco);
						$connection_insert -> bindValue(":cidade",$cidade);
						$connection_insert -> bindValue(":estado",$estado);
						$connection_insert -> bindValue(":cep",$cep);
						$connection_insert -> bindValue(":situacao",utf8_decode('Não cadastrado'));
					$connection_insert -> execute();

					$_SESSION['certos']++;
				} else {
					$_SESSION['errados']++;
				}
			}

			/*====================================================================================================================================================================*/

			public function mostrar_todos_pedidos_resolvidos_alunos($id){
				$con= new Conexao();
				$conn= $con->Connection();

				try{
					$sql= "SELECT id_pedidos_resolvidos, id_pedidos_pendentes, situacao, pedidos_resolvidos.data_hora, numero_SEI, titulo_solicitacao FROM pedidos_resolvidos
								INNER JOIN pedidos_pendentes ON id_pedidos_pendente_fk=id_pedidos_pendentes INNER JOIN discentes ON id_discente_fk=id_discente INNER JOIN solicitacoes_setor_campus
								ON id_solicitacao_setor_campus_fk=id_solicitacao_setor_campus INNER JOIN solicitacoes ON id_solicitacao=id_solicitacao_fk WHERE status=1
								AND id_discente_fk=:id_discente ORDER BY data_hora DESC";

					$connectionn= $conn->prepare($sql);
					$connectionn->bindValue(':id_discente',$id);
					$connectionn->execute();

					$vl= $connectionn->rowCount();

					if ($vl>0) {
						$dados= $connectionn->fetchAll();

						for ($i=0; $i < $vl; $i++) {
							if	($dados[$i]['situacao']=='recusado'){
								$class= 'danger';
							}
							if	($dados[$i]['situacao']=='aceito'){
								$class= 'success';
							}
							echo "
								<div class='alert alert-$class' id='conc'>
									<strong>Nº ".$dados[$i]['id_pedidos_pendentes']." </strong> &nbsp; ".utf8_encode($dados[$i]['titulo_solicitacao'])."
									- ".date('d/m/Y H:i:s', strtotime($dados[$i]['data_hora']))."
								</div>
							";
						}
	 				}
				} catch(Exception $e) {
					modal('ERRO', 'ERRO NO SISTEMA', '641218', '0');
				}
			}

			/*====================================================================================================================================================================*/

			public function mostrar_todos_pedidos_pendentes_alunos($id){
				$con= new Conexao();
				$conn= $con->Connection();

				try{
					$sql= "SELECT id_pedidos_pendentes, data_hora, titulo_solicitacao FROM pedidos_pendentes INNER JOIN discentes on id_discente_fk=id_discente
								INNER JOIN solicitacoes_setor_campus ON id_solicitacao_setor_campus_fk=id_solicitacao_setor_campus INNER JOIN solicitacoes ON id_solicitacao=id_solicitacao_fk
								WHERE status=0 AND id_discente_fk=:id_discente ORDER BY data_hora DESC";

					$connectionn= $conn->prepare($sql);
					$connectionn->bindValue(':id_discente',$id);
					$connectionn->execute();

					$vl= $connectionn->rowCount();

					if ($vl>0) {
						$dados= $connectionn->fetchAll();

						for ($i=0; $i < $vl; $i++) {
							echo "
							<div class='alert alert-info'>
								<strong>Nº ".$dados[$i]['id_pedidos_pendentes']." </strong> &nbsp; ".utf8_encode($dados[$i]['titulo_solicitacao'])."
								 - ".date('d/m/Y H:i:s', strtotime($dados[$i]['data_hora']))."
							</div>

							";
						}
					} else {
						echo "
							<div class='alert'>
								<strong>Nenhuma solicitação pendente</strong>
							</div>
						";
					}
				} catch(Exception $e) {
					modal('ERRO', 'ERRO NO SISTEMA', '641218', '0');
				}
			}

			/*====================================================================================================================================================================*/



	}
?>
