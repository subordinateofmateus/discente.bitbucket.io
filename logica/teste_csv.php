<?php
  session_start();

  require_once 'CRUD.class.php';

  $CRUD = new CRUD();
?>

<form action="" method="POST" enctype="multipart/form-data">
  <label>Comprovante</label>
  <input type="file" name="arquivo" class="form-control">
  <br>
  <button type="submit" name="en">Enviar</button>
</form>

<?php
	if (isset($_POST['en'])) {
		$formatosPermitidos = array("txt","csv","odt", "doc");

		$extencao = pathinfo($_FILES['arquivo']['name'], PATHINFO_EXTENSION);

    if (in_array($extencao, $formatosPermitidos)) {
  		$temporario = $_FILES['arquivo']['tmp_name'];

      $objeto = fopen($temporario, 'r');

      $_SESSION['certos'] = 0;
      $_SESSION['errados'] = 0;

      while (($dados = fgetcsv($objeto, 1000, ';')) !== FALSE) {
        $matricula = utf8_decode($dados[0]);
        $nome = utf8_decode($dados[1]);
        $cpf = utf8_decode($dados[2]);
        $data_nascimento = utf8_decode($dados[3]);
        $fone = utf8_decode($dados[4]);
        $endereco = utf8_decode($dados[5]);
        $cidade = utf8_decode($dados[6]);
        $estado = utf8_decode($dados[7]);
        $cep = utf8_decode($dados[8]);

        $CRUD -> cadastrar_aluno($matricula,$nome, $cpf, $data_nascimento, $fone, $endereco, $cidade, $estado, $cep);
      }

      if ($_SESSION['certos'] != 0) {
        $titulo_modal =  "Aluno(s) cadastrado(s) com sucesso!";
        $informacao_modal = "Novo(s) ".$_SESSION['certos']." Aluno(s) registrado! (Total de já matriculados: ".$_SESSION['errados'].")";
        $cor_modal = "1BAC91";
      } else {
        $titulo_modal =  "Nenhum Aluno cadastrado!";
        $informacao_modal = "Matriculas já registradas";
        $cor_modal = "641218";
      }

      modal($titulo_modal, $informacao_modal, $cor_modal, '0');
    }
	}
?>
