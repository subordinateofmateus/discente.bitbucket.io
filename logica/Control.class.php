	<link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="color/default.css" rel="stylesheet">
	<link rel="shortcut icon" href="img/favicon.ico">
	<script src="js/jquery.js"></script>
	<script src="js/jquery.scrollTo.js"></script>
	<script src="js/jquery.nav.js"></script>
	<script src="js/jquery.localScroll.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/isotope.js"></script>
	<script src="js/jquery.flexslider.js"></script>
	<script src="js/inview.js"></script>
	<script src="js/animate.js"></script>
	<script src="js/custom.js"></script>

<?php
	require_once 'Conexao.class.php';
	require_once 'enviaremail.php';
	require_once 'modal.php';

	class Control{

		public function Logar_discente($matricula, $senha){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT * FROM discentes WHERE :matricula=matricula_fk";

			$connection= $conn->prepare($sql);
				$connection->bindValue(":matricula", $matricula);
			$connection->execute();

			$vl= $connection->rowCount();

			if($vl>0){
				$sql_2= "SELECT id_discente,email,senha,matricula, id_curso_campus_fk, matriculados.id_campus_fk,id_curso_fk, turno, nome_discente, semestre_insercao, rg, cpf, data_nascimento,
								fone, endereco,	cidade, estado, CEP, situacao_sistema, id_instituto_fk, nome_instituto, nome_curso FROM discentes INNER JOIN cursos_campus
								ON id_curso_campus_fk=id_curso_campus INNER JOIN cursos ON id_curso=id_curso_fk	INNER JOIN matriculados ON matricula=matricula_fk INNER JOIN campus
								ON id_campus=matriculados.id_campus_fk INNER JOIN instituto ON id_instituto=id_instituto_fk WHERE matricula_fk=:matricula AND senha=:senha";

				$connection_2= $conn->prepare($sql_2);
					$connection_2->bindValue(":matricula", $matricula);
					$connection_2->bindValue(":senha", $senha);
				$connection_2->execute();

				$vl_2= $connection_2->rowCount();

				if ($vl_2>0) {
					$discente= $connection_2->fetchAll();

					$_SESSION['logado']=1;

					$_SESSION['id']= $discente[0]['id_discente'];
					$_SESSION['nome']= $discente[0]['nome_discente'];
					$_SESSION['email']= $discente[0]['email'];
					$_SESSION['senha']= $discente[0]['senha'];
					$_SESSION['cpf']= $discente[0]['cpf'];
					$_SESSION['rg']= $discente[0]['rg'];
					$_SESSION['matricula']= $discente[0]['matricula'];
					$_SESSION['semestre_insercao']= $discente[0]['semestre_insercao'];
					$_SESSION['fone']= $discente[0]['fone'];
					$_SESSION['nome_curso']= $discente[0]['nome_curso'];
					$_SESSION['turno_curso']= $discente[0]['turno'];
					$_SESSION['endereco']= $discente[0]['endereco'];
					$_SESSION['estado']= $discente[0]['estado'];
					$_SESSION['CEP']= $discente[0]['CEP'];
					$_SESSION['cidade']= $discente[0]['cidade'];
					$_SESSION['data_nascimento']= date('d/m/Y', strtotime($discente[0]['data_nascimento']));
					$_SESSION['id_curso']= $discente[0]['id_curso_fk'];
					$_SESSION['id_campus']= $discente[0]['id_campus_fk'];
					$_SESSION['id_curso_campus']= $discente[0]['id_curso_campus_fk'];
					$_SESSION['id_instituto']= $discente[0]['id_instituto_fk'];
					$_SESSION['nome_instituto']= $discente[0]['nome_instituto'];

					header('location: ../menu.php');
				} else {
					modal('Senha incorreta', 'Tente novamente!', '641218', '../index.html');
				}
			} else {
				modal('Matrícula incorreta', 'Tente novamente!', '641218', '../index.html');
			}
		}

		/*=================================================================================================================================================================================*/

		public function Logar_servidor($id_servidor, $senha){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT * FROM servidores WHERE id_servidor=:id_servidor";

			$connection= $conn->prepare($sql);
				$connection->bindValue(":id_servidor", $id_servidor);
			$connection->execute();

			$vl= $connection->rowCount();

			if($vl>0){
				$sql_2= "SELECT id_servidor, senha, nome_servidor, cpf, email_servidor, id_setor_campus_fk, id_curso_campus_fk, setores_campus.id_campus_fk, id_curso_fk, nome_curso, turno,
								id_setor_fk, id_instituto_fk, nome_instituto FROM servidores INNER JOIN cursos_campus ON id_curso_campus_fk=id_curso_campus INNER JOIN cursos ON id_curso=id_curso_fk
								INNER JOIN setores_campus ON id_setor_campus=id_setor_campus_fk INNER JOIN campus ON id_campus=setores_campus.id_campus_fk INNER JOIN instituto
								ON id_instituto=id_instituto_fk WHERE id_servidor=:id_servidor AND senha=:senha";

				$connection_2= $conn->prepare($sql_2);
					$connection_2->bindValue(":id_servidor", $id_servidor);
					$connection_2->bindValue(":senha", $senha);
				$connection_2->execute();

				$vl_2= $connection_2->rowCount();

				if ($vl_2>0) {
					$servidor= $connection_2->fetchAll();

					$_SESSION['logado']=2;

					$_SESSION['id']= $servidor[0]['id_servidor'];
					$_SESSION['senha']= $servidor[0]['senha'];
					$_SESSION['nome']= $servidor[0]['nome_servidor'];
					$_SESSION['cpf']= $servidor[0]['cpf'];
					$_SESSION['email']= $servidor[0]['email_servidor'];
					$_SESSION['id_setor_campus']= $servidor[0]['id_setor_campus_fk'];
					$_SESSION['id_curso_campus']= $servidor[0]['id_curso_campus_fk'];
					$_SESSION['id_campus']= $servidor[0]['id_campus_fk'];
					$_SESSION['id_curso']= $servidor[0]['id_curso_fk'];
					$_SESSION['nome_curso']= $servidor[0]['nome_curso'];
					$_SESSION['turno_curso']= $servidor[0]['turno'];
					$_SESSION['id_setor']= $servidor[0]['id_setor_fk'];
					$_SESSION['id_instituto']= $servidor[0]['id_instituto_fk'];
					$_SESSION['nome_instituto']= $servidor[0]['nome_instituto'];

					header('Location: ../adm/servidor.php');
				} else {
					modal('Senha incorreta', 'Tente novamente!', '641218', '../index.html');
				}

			} else {
				modal('Identificação incorreta', 'Tente novamente!', '641218', '../index.html');
			}
		}

		/*=================================================================================================================================================================================*/

		public function Logar_adm($id_adm, $senha){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT * FROM adm WHERE id_adm=:id_adm";

			$connection= $conn->prepare($sql);
				$connection->bindValue(":id_adm", $id_adm);
			$connection->execute();

			$vl= $connection->rowCount();

			if($vl>0){
				$sql_2= "SELECT id_adm, id_campus_fk, nome_adm, senha, id_instituto_fk, nome_instituto FROM adm INNER JOIN campus ON id_campus=id_campus_fk INNER JOIN instituto
								ON id_instituto=id_instituto_fk WHERE id_adm=:id_adm AND senha=:senha";

				$connection_2= $conn->prepare($sql_2);
					$connection_2->bindValue(":id_adm", $id_adm);
					$connection_2->bindValue(":senha", $senha);
				$connection_2->execute();

				$vl_2= $connection_2->rowCount();

				if ($vl_2>0) {
					$adm = $connection_2->fetchAll();

					$_SESSION['logado']= 3;

					$_SESSION['id']= $adm[0]['id_adm'];
					$_SESSION['id_campus']= $adm[0]['id_campus_fk'];
					$_SESSION['nome_adm']= $adm[0]['nome_adm'];
					$_SESSION['senha']= $adm[0]['senha'];
					$_SESSION['id_instituto']= $adm[0]['id_instituto_fk'];
					$_SESSION['nome_instituto']= $adm[0]['nome_instituto'];

					header('Location: ../adm/admin.php');
				} else {
					modal('Senha incorreta', 'Tente novamente!', '641218', '../index.html');
				}
			} else {
				modal('Identificação incorreta', 'Tente novamente!', '641218', '../index.html');
			}
		}

		/*=================================================================================================================================================================================*/

		public function realizar_solicitacao($id_solicitacao_setor_campus, $id_discente, $data_falta, $justificativa, $nome_arquivo, $disciplina, $id_curso_campus_servidor, $id_campus, $id_instituto){
			$con= new Conexao();
			$conn= $con->Connection();

			$sql= "SELECT id_servidor, nome_servidor, email_servidor, titulo_solicitacao FROM solicitacoes_setor_campus INNER JOIN servidores
						ON servidores.id_setor_campus_fk=solicitacoes_setor_campus.id_setor_campus_fk INNER JOIN solicitacoes ON id_solicitacao=id_solicitacao_fk INNER JOIN cursos_campus
						ON id_curso_campus=id_curso_campus_fk INNER JOIN campus ON id_campus=id_campus_fk INNER JOIN instituto ON id_instituto=id_instituto_fk
						WHERE id_solicitacao_setor_campus=:id_solicitacao_setor_campus AND servidores.id_curso_campus_fk=:id_curso_campus AND id_campus=:id_campus AND id_instituto=:id_instituto";

			$connection= $conn->prepare($sql);
				$connection->bindValue(':id_solicitacao_setor_campus',$id_solicitacao_setor_campus);
				$connection->bindValue(':id_curso_campus',$id_curso_campus_servidor);
				$connection->bindValue(':id_campus',$id_campus);
				$connection->bindValue(':id_instituto',$id_instituto);
			$connection->execute();

			$vl= $connection->rowCount();

			if ($vl>0) {
				$dados= $connection->fetchAll();

				$data= date('Y-m-d H:i:s');
				$d= date('d');
				$m= date('m');
				$Y= date('Y');
				$H= date('H');
				$i= date('i');
				$s= date('s');

				$corpoMensagem = "
				<link href='https://fonts.googleapis.com/css?family=Roboto&display=swap' rel='stylesheet'>
				<style>
				#form-control {
				  display: block;
				  width: 100%;
				  height: calc(1.5em + 0.75rem + 0.25rem);
				  padding: 0.375rem 0.75rem;
				  font-size: 1rem;
				  font-weight: 400;
				  line-height: 1.5;
				  color: #495057;
				  background-color: #fff;
				  background-clip: padding-box;
				  border: 0.125rem solid #ced4da;
				  border-radius: 0.5rem;
				  -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
				  transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
				  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
				  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
				}
				.col-3 {
				  -webkit-box-flex: 0;
				  -ms-flex: 0 0 25%;
				  flex: 0 0 25%;
				  max-width: 25%;
				}
				</style>
				<div style='height:100%; background-image: url(https://66.media.tumblr.com/c4f5d6caa31231ff79120b05151b9d3b/tumblr_pxddlhwmfa1yu5agro1_1280.png); background-size:100%;
				background-repeat:no-repeat;
				background-position: center center; margin-left: 20%; margin-right:20%;margin-top: 5px; font-size:20px; font-family: Roboto, sans-serif;'>

				<center>
				<img src='https://66.media.tumblr.com/08ec0c476ed62aaf7a65b9445b6c3790/tumblr_pwajvl5QKO1yu5agro1_1280.png' style='height: 110px; width: 420px; margin-top: 25px;'><br>
										<h2 style='font-family: Roboto, sans-serif; margin-top: 10px;'>".$dados[0]['titulo_solicitacao']."</h2></center>
										<div style='margin-left:20%; margin-right:20%;'>
										<b>Nome: </b>".$_SESSION['nome']." <br>
										<b>Matrícula: </b> ".$_SESSION['matricula']." <br>
										<b>Curso: </b> ".$_SESSION['nome_curso']."<br>
										<b>Turno: </b> ".$_SESSION['turno_curso']."<br>
										<b>Endereço: </b> ".$_SESSION['endereco']."<br>
										<b>Telefone: </b> ".$_SESSION['fone']."<br><br>
										<div class='form-group'>
										<label><b>JUSTIFICATIVA:</b></label><br>
										<textarea id='form-control'>$justificativa</textarea>
										</div>
										</div>
										<br>
										</div>
				";

				$sql_pedido = "SELECT * FROM pedidos_pendentes ORDER BY id_pedidos_pendentes DESC LIMIT 1";
				$connection_pedido= $conn->prepare($sql_pedido);
				$connection_pedido->execute();
				$dados_pedido= $connection_pedido->fetchAll();

					if (smtpmailer($dados[0]['email_servidor'], 'kelpgf@gmail.com', 'IFCE MARANGUAPE', "PROTOCOLO DISCENTE - PEDIDO ".$dados_pedido[0][0]."/".$Y, $corpoMensagem)) {

							$sql_pedidos_pendentes= "INSERT INTO pedidos_pendentes VALUES (null, :id_solicitacao, :id_discente, :data , :justificativa, 0,
							:disciplina, :data_falta)";

							$connection_pedidos_pendentes= $conn->prepare($sql_pedidos_pendentes);
								$connection_pedidos_pendentes->bindValue(':id_solicitacao',$id_solicitacao_setor_campus);
								$connection_pedidos_pendentes->bindValue(':id_discente',$id_discente);
								$connection_pedidos_pendentes->bindValue(':data',$data);
								$connection_pedidos_pendentes->bindValue(':justificativa',$justificativa);
								$connection_pedidos_pendentes->bindValue(':disciplina',$disciplina);
								$connection_pedidos_pendentes->bindValue(':data_falta',$data_falta);
							$connection_pedidos_pendentes->execute();

							$numero_protocolo= $conn->lastInsertId();

							$sql_arquivo= "INSERT INTO arquivos_pedidos VALUES(null, :nome_arquivo, :id_pedido_pendente)";

							$connection_arquivo= $conn->prepare($sql_arquivo);
								$connection_arquivo->bindValue(':nome_arquivo',$nome_arquivo);
								$connection_arquivo->bindValue(':id_pedido_pendente',$numero_protocolo);
							$connection_arquivo->execute();

							modal('Solicitação enviada com sucesso!','Aguarde a resposta do servidor!','1BAC91', '0');
					} else {
						modal('Erro ao concluir a solicitaçao!','Tente novamente','641218', '0');
					}
			} else {
				modal('Erro ao concluir a solicitaçao!','Sem servidor','641218', '0');
			}
		}

		/*=================================================================================================================================================================================*/

		public function _() {

		}

		/*=================================================================================================================================================================================*/

	}
?>
