<?php
  session_start();
require_once 'logica/CRUD.class.php';
$CRUD = new CRUD();
$_SESSION['id_campus'] = 32;
$_SESSION['id_instituto'] = 1;
 ?>

<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <meta charset="utf-8">
    <title>Teste</title>
  </head>
  <body>
    <br>
    <div class="container col-md-7">
      <div class="row">

      <input type="text" id="palavra" class="form-control col-4" placeholder="Buscar por...">
      <button type="button" class="btn btn-success col-2" id="buscar">
        Buscar
      </button>
    </div>
  <br>
    <div id="dados">
      <table class="table">
        <thead>
          <th>Id</th>
          <th>Nome</th>
          <th>Email</th>
          <th>CPF</th>
          <th>Setor</th>
          <th>Curso</th>
          <th>Ação</th>
        </thead>
        <tbody>
          <?php
          $CRUD->select_servidor();
           ?>
        </tbody>
      </table>
    </div>
  </div>
  </body>
</html>
<script>
  function buscar(){
    $.ajax ({
      //Configurações
      type: 'POST',
      dataType: 'html',
      url: 'busca.php',
      beforeSend: function(){
        $("#dados").html("Carregando...");
      },
      success : function(msg)
      {
        $("#dados").html(msg);
      }
    });
  }

  $('#buscar').click( function() {
   buscar()
  });

</script>
