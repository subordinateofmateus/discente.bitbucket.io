<?php
  session_start();

  require_once 'logica/Control.class.php';
  require_once 'logica/CRUD.class.php';

  $CRUD= new CRUD();
  $Control= new Control();

  $id_discente = $_SESSION['id'];


?>
<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Central de serviços</title>

  <!-- Custom fonts for this theme -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

  <!-- Theme CSS -->
  <link href="css/freelancer.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg bg-dark text-uppercase" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.html"><img src="img/logo.png" width="60" height="75" style="margin-top: -17px; margin-bottom: -17px;"></a>
      <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">Notificações</a>
          </li>
           <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle py-3 px-0 px-lg-3" href="#" id="userDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php echo $_SESSION['nome']; ?> </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="trocar_senha.php">
                  <i class="fas fa-lock fa-sm fa-fw mr-2 text-gray-400"></i>
                  Trocar senha
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="logica/sair.php">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sair
                </a>
              </div>
            </li>
        </ul>
      </div>
    </div>
  </nav>


  <!-- Portfolio Section -->
  <section class="page-section portfolio" id="portfolio">
    <div class="container">

      <!-- Portfolio Section Heading -->
      <h2 class="page-section-heading text-center text-uppercase text-secondary mb-5">Menu de Opções</h2>
      <!-- Portfolio Grid Items -->
      <div class="row">

        <!-- Portfolio Item 1 -->
        <div class="col-md-6 col-lg-4">
          <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#solicitacao1">
            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
              <div class="portfolio-item-caption-content text-center text-white">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/thumb/segunda_c.jpg" alt="">
          </div>
        </div>

        <!-- Portfolio Item 2 -->
        <div class="col-md-6 col-lg-4">
          <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#solicitacao2">
            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
              <div class="portfolio-item-caption-content text-center text-white">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/thumb/justificar_f.jpg" alt="">
          </div>
        </div>

        <!-- Portfolio Item 3 -->
        <div class="col-md-6 col-lg-4">
          <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#solicitacao3">
            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
              <div class="portfolio-item-caption-content text-center text-white">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/thumb/t_disciplina.jpg" alt="">
          </div>
        </div>

        <!-- Portfolio Item 4 -->
        <div class="col-md-6 col-lg-4">
          <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#solicitacao4">
            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
              <div class="portfolio-item-caption-content text-center text-white">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/thumb/t_matricula.jpg" alt="">
          </div>
        </div>

        <!-- Portfolio Item 5 -->
        <div class="col-md-6 col-lg-4">
          <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#solicitacao5">
            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
              <div class="portfolio-item-caption-content text-center text-white">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/thumb/colacao.jpg" alt="">
          </div>
        </div>

        <!-- Portfolio Item 6 -->
        <div class="col-md-6 col-lg-4">
          <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#solicitacao6">
            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
              <div class="portfolio-item-caption-content text-center text-white">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            <img class="img-fluid" src="img/thumb/cancelamento_m.jpg" alt="">
          </div>
        </div>
      </div>
      <!-- /.row -->
        <div class="text-center mt-4">
        <a href="" class="btn btn-xl btn-primary" data-toggle="modal" data-target="#outra">
          OUTRA SOLICITAÇÃO
        </a>
      </div>
    </div>
  </section>

  <!-- About Section -->
  <section class="page-section bg-primary text-white mb-0" id="about">
    <div class="container">

      <!-- About Section Heading -->
      <h2 class="page-section-heading text-center text-uppercase text-white mb-4">Notificações</h2>



      <!-- About Section Content -->
      <div class="row">
          <div class="col-lg-6 ml-auto mb-4 mt-4">
                  <div class="card shadow">
                    <div class="card-header">
                      <h6 class="m-0 font-weight-bold text-dark">Pedidos pendentes</h6>
                    </div>
                    <div class="card-body">
                    <?php
                    $CRUD->mostrar_pedidos_pendentes_alunos($id_discente); ?>
                   </div>
                   <div class="card-footer">
                    <div class="float-right">
                    <a href="" style="font-size: 18px;">Ver todas <i class="fas fa-angle-double-right"> </i></a>
                    </div>
                   </div>
                   </div>
                </div>

          <div class="col-lg-6 mr-auto mt-4">
                          <div class="card shadow">
                    <div class="card-header">
                      <h6 class="m-0 font-weight-bold text-dark">Pedidos respondidos</h6>
                    </div>
                    <div class="card-body">
                     <?php
                    $CRUD->mostrar_pedidos_resolvidos_alunos($id_discente); ?>
                   </div>
                   <div class="card-footer">
                    <div class="float-right">
                    <a href="" style="font-size: 18px;">Ver todas  <i class="fas fa-angle-double-right"> </i></a>
                    </div>
                  </div>
                   </div>
            </div>

      <!-- About Section Button -->


    </div>
  </section>
  <!-- Footer -->
  <footer class="footer text-center">
    <div class="container">
      <div class="row">

        <!-- Footer Location -->
        <div class="col-lg-4 mb-5 mb-lg-0">
          <h4 class="text-uppercase mb-4">Location</h4>
          <p class="lead mb-0">2215 John Daniel Drive
            <br>Clark, MO 65243</p>
        </div>

        <!-- Footer Social Icons -->
        <div class="col-lg-4 mb-5 mb-lg-0">
          <h4 class="text-uppercase mb-4">Around the Web</h4>
          <a class="btn btn-outline-light btn-social mx-1" href="#">
            <i class="fab fa-fw fa-facebook-f"></i>
          </a>
          <a class="btn btn-outline-light btn-social mx-1" href="#">
            <i class="fab fa-fw fa-twitter"></i>
          </a>
          <a class="btn btn-outline-light btn-social mx-1" href="#">
            <i class="fab fa-fw fa-linkedin-in"></i>
          </a>
          <a class="btn btn-outline-light btn-social mx-1" href="#">
            <i class="fab fa-fw fa-dribbble"></i>
          </a>
        </div>

        <!-- Footer About Text -->
        <div class="col-lg-4">
          <h4 class="text-uppercase mb-4">About Freelancer</h4>
          <p class="lead mb-0">Freelance is a free to use, MIT licensed Bootstrap theme created by
            <a href="http://startbootstrap.com">Start Bootstrap</a>.</p>
        </div>

      </div>
    </div>
  </footer>

  <!-- Copyright Section -->
  <section class="copyright py-4 text-center text-white">
    <div class="container">
      <small>Copyright &copy; Your Website 2019</small>
    </div>
  </section>

  <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
  <div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
      <i class="fa fa-chevron-up"></i>
    </a>
  </div>

  <!-- Portfolio Modals -->


 <!-- Portfolio Modal 1 -->
  <div class="portfolio-modal modal fade" id="solicitacao1" tabindex="-1" role="dialog" aria-labelledby="solicitacao1Label" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="container">
            <div class="row justify-content-center">
              <h3>Segunda chamada de prova</h3>
              <div class="col-lg-12">
                <!-- Portfolio Modal - Title -->
                    <div class="col-lg-12 mx-auto">
                      <form action='' method='POST' enctype="multipart/form-data">
                        <input type="hidden" name="id_solicitacao" value='4'>
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-3 pb-1">
                            <label>Disciplina</label>
                            <input class="form-control text-uppercase" name="materia" type="text" placeholder="Disciplina" required="required" data-validation-required-message="Please enter your name.">
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                        <div class="control-group">
                          <div class="form-group controls mt-2 mb- pb-0">
                            <label style="font-size: 1.1rem;">Data da Falta</label>
                            <input class="form-control" name="data_falta" type="date" required="required" style="border: transparent; font-size: 1.5em;">
                          </div>
                            <div style="border-top: #e9ecef solid 1px;"></div>
                        </div>
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-3 mt-2 pb-1">
                            <label>Justificativa</label>
                            <textarea class="form-control text-uppercase" name="justificativa" rows="5" placeholder="Justificativa" required="required" data-validation-required-message="Please enter a message."></textarea>
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                          <div class="control-group">
                            <div class="form-group controls mt-2 mb- pb-0">
                              <label style="font-size: 1.1rem;">Comprovante</label>
                                <div class="custom-file">
                                  <input type="file" class="custom-file-input" id="customFileLang" lang="pt-br" name="comprovante">
                                  <label class="custom-file-label" for="customFileLang">Selecionar arquivo</label>
                                </div>
                            </div>
                           </div>
                          <button class="btn btn-secondary btn-sm mt-0 mb-4">Baixar comprovante</button>
                          <div style="border-top: #e9ecef solid 1px;"></div>
                        <br>
                          <button type="submit" class="btn btn-primary btn-lg" name="enviar_solicitacao">Enviar</button>
                      </form>
                    </div>


                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Portfolio Modal 2 -->
  <div class="portfolio-modal modal fade" id="solicitacao2" tabindex="-1" role="dialog" aria-labelledby="solicitacao1Label" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="container">
            <div class="row justify-content-center">
              <h3>Justificar falta</h3>
              <div class="col-lg-12">
                <!-- Portfolio Modal - Title -->
                    <div class="col-lg-12 mx-auto">
                      <form action='' method='POST' enctype="multipart/form-data">
                        <input type="hidden" name="id_solicitacao" value='3'>
                        <div class="control-group">
                          <div class="form-group controls mt-2 mb- pb-0">
                            <label style="font-size: 1.1rem;">Data da Falta</label>
                            <input class="form-control" type="date" name="data_falta" required="required" style="border: transparent; font-size: 1.5em;">
                          </div>
                            <div style="border-top: #e9ecef solid 1px;"></div>
                        </div>
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-3 mt-2 pb-1">
                            <label>Justificativa</label>
                            <textarea class="form-control text-uppercase" name="justificativa" rows="5" placeholder="Justificativa" required="required" data-validation-required-message="Please enter a message."></textarea>
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                            <div class="control-group">
                            <div class="form-group controls mt-2 mb- pb-0">
                              <label style="font-size: 1.1rem;">Comprovante</label>
                                <div class="custom-file">
                                  <input type="file" class="custom-file-input" name="comprovante" id="customFileLang" lang="pt-br">
                                  <label class="custom-file-label" for="customFileLang">Selecionar arquivo</label>
                                </div>
                            </div>
                           </div>
                          <button class="btn btn-secondary btn-sm mt-0 mb-4">
                            <i class="fas fa-file"> &nbsp;</i>
                          Baixar comprovante</button>
                          <div style="border-top: #e9ecef solid 1px;"></div>
                        <br>
                          <button type="submit" class="btn btn-primary btn-lg" name="enviar_solicitacao">Enviar</button>
                      </form>
                    </div>


                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Portfolio Modal 3 -->
  <div class="portfolio-modal modal fade" id="solicitacao3" tabindex="-1" role="dialog" aria-labelledby="solicitacao1Label" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="container">
            <div class="row justify-content-center">
              <h3>Trancamento da disciplina</h3>
              <div class="col-lg-12">
                <!-- Portfolio Modal - Title -->
                    <div class="col-lg-12 mx-auto">
                      <form action='' method='POST' enctype="multipart/form-data">
                        <input type="hidden" name="id_solicitacao" value='5'>
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-3 pb-1">
                            <label>Disciplina</label>
                            <input class="form-control text-uppercase" name="materia" type="text" placeholder="Disciplina" required="required" data-validation-required-message="Please enter your name.">
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-3 mt-2 pb-1">
                            <label>Justificativa</label>
                            <textarea class="form-control text-uppercase" name="justificativa" rows="5" placeholder="Justificativa" required="required" data-validation-required-message="Please enter a message."></textarea>
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                            <div class="control-group">
                            <div class="form-group controls mt-2 mb- pb-0">
                              <label style="font-size: 1.1rem;">Comprovante</label>
                                <div class="custom-file">
                                  <input type="file" class="custom-file-input" id="customFileLang" lang="pt-br" name="comprovante">
                                  <label class="custom-file-label" for="customFileLang">Selecionar arquivo</label>
                                </div>
                            </div>
                           </div>
                          <button class="btn btn-secondary btn-sm mt-0 mb-4">Baixar comprovante</button>
                          <div style="border-top: #e9ecef solid 1px;"></div>
                        <br>
                          <button type="submit" class="btn btn-primary btn-lg" name="enviar_solicitacao">Enviar</button>
                      </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <!-- Portfolio Modal 4 -->
  <div class="portfolio-modal modal fade" id="solicitacao4" tabindex="-1" role="dialog" aria-labelledby="solicitacao1Label" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="container">
            <div class="row justify-content-center">
              <h3>Trancamento de matrícula</h3>
              <div class="col-lg-12">
                <!-- Portfolio Modal - Title -->
                    <div class="col-lg-12 mx-auto">
                      <form action='' method='POST' enctype="multipart/form-data">
                        <input type="hidden" name="id_solicitacao" value='6'>
                        <input type="hidden" name="id_curso" value='0'>
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-3 pb-1">
                            <label>Justificativa</label>
                            <textarea class="form-control text-uppercase" name="justificativa" rows="5" placeholder="Justificativa" required="required" data-validation-required-message="Please enter a message."></textarea>
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                            <div class="control-group">
                            <div class="form-group controls mt-2 mb- pb-0">
                              <label style="font-size: 1.1rem;">Comprovante</label>
                                <div class="custom-file">
                                  <input type="file" class="custom-file-input" id="customFileLang" lang="pt-br" name="comprovante">
                                  <label class="custom-file-label" for="customFileLang">Selecionar arquivo</label>
                                </div>
                            </div>
                           </div>
                          <button class="btn btn-secondary btn-sm mt-0 mb-4">Baixar comprovante</button>
                          <div style="border-top: #e9ecef solid 1px;"></div>
                        <br>
                          <button type="submit" class="btn btn-primary btn-lg" name="enviar_solicitacao">Enviar</button>
                      </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <!-- Portfolio Modal 5 -->
  <div class="portfolio-modal modal fade" id="solicitacao5" tabindex="-1" role="dialog" aria-labelledby="solicitacao1Label" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="container">
            <div class="row justify-content-center">
              <h3>Colação de Grau especial</h3>
              <div class="col-lg-12">
                <!-- Portfolio Modal - Title -->
                    <div class="col-lg-12 mx-auto">
                      <form action='' method='POST' enctype="multipart/form-data">
                        <input type="hidden" name="id_solicitacao" value='2'>
                        <input type="hidden" name="id_curso" value='0'>
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-3 pb-1">
                            <label>Justificativa</label>
                            <textarea class="form-control text-uppercase" name="justificativa" rows="5" placeholder="Justificativa" required="required" data-validation-required-message="Please enter a message."></textarea>
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                            <div class="control-group">
                            <div class="form-group controls mt-2 mb- pb-0">
                              <label style="font-size: 1.1rem;">Comprovante</label>
                                <div class="custom-file">
                                  <input type="file" class="custom-file-input" id="customFileLang" lang="pt-br" name="comprovante">
                                  <label class="custom-file-label" for="customFileLang">Selecionar arquivo</label>
                                </div>
                            </div>
                           </div>
                          <button class="btn btn-secondary btn-sm mt-0 mb-4" >Baixar comprovante</button>
                          <div style="border-top: #e9ecef solid 1px;"></div>
                        <br>
                          <button type="submit" class="btn btn-primary btn-lg" name="enviar_solicitacao">Enviar</button>
                      </form>
                    </div>


                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <!-- Portfolio Modal 6 -->
  <div class="portfolio-modal modal fade" id="solicitacao6" tabindex="-1" role="dialog" aria-labelledby="solicitacao1Label" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="container">
            <div class="row justify-content-center">
              <h3>Cancelamento de matrícula</h3>
              <div class="col-lg-12">
                <!-- Portfolio Modal - Title -->
                    <div class="col-lg-12 mx-auto">
                      <form action='' method='POST' enctype="multipart/form-data">
                        <input type="hidden" name="id_solicitacao" value='1'>
                        <input type="hidden" name="id_curso" value='0'>
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-3 pb-1">
                            <label>Justificativa</label>
                            <textarea class="form-control text-uppercase" name="justificativa" rows="5" placeholder="Justificativa" required="required" data-validation-required-message="Please enter a message."></textarea>
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                            <div class="control-group">
                            <div class="form-group controls mt-2 mb- pb-0">
                              <label style="font-size: 1.1rem;">Comprovante</label>
                                <div class="custom-file">
                                  <input type="file" class="custom-file-input" id="customFileLang" lang="pt-br" name="comprovante">
                                  <label class="custom-file-label" for="customFileLang">Selecionar arquivo</label>
                                </div>
                            </div>
                           </div>
                          <button class="btn btn-secondary btn-sm mt-0 mb-4">Baixar comprovante</button>
                          <div style="border-top: #e9ecef solid 1px;"></div>
                        <br>
                          <button type="submit" class="btn btn-primary btn-lg" name="enviar_solicitacao">Enviar</button>
                      </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Portfolio Modal 6 -->
  <div class="portfolio-modal modal fade" id="outra" tabindex="-1" role="dialog" aria-labelledby="solicitacao1Label" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="container">
            <div class="row justify-content-center">
              <h3>Outra solicitação</h3>
              <div class="col-lg-12">
                <!-- Portfolio Modal - Title -->
                    <div class="col-lg-12 mx-auto">
                      <form action='' method='POST' enctype="multipart/form-data">
                        <input type="hidden" name="id_solicitacao" value='1'>
                        <input type="hidden" name="id_curso" value='0'>
                        <div class="control-group">
                          <div class="form-group floating-label-form-group controls mb-3 pb-1">
                            <label>Justificativa</label>
                            <textarea class="form-control text-uppercase" name="justificativa" rows="5" placeholder="Justificativa" required="required" data-validation-required-message="Please enter a message."></textarea>
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                            <div class="control-group">
                            <div class="form-group controls mt-2 mb- pb-0">
                              <label style="font-size: 1.1rem;">Comprovante</label>
                                <div class="custom-file">
                                  <input type="file" class="custom-file-input" id="customFileLang" lang="pt-br" name="comprovante">
                                  <label class="custom-file-label" for="customFileLang">Selecionar arquivo</label>
                                </div>
                            </div>
                           </div>
                          <button class="btn btn-secondary btn-sm mt-0 mb-4">Baixar comprovante</button>
                          <div style="border-top: #e9ecef solid 1px;"></div>
                        <br>
                          <button type="submit" class="btn btn-primary btn-lg" name="enviar_solicitacao">Enviar</button>
                      </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact Form JavaScript -->
  <script src="js/jqBootstrapValidation.js"></script>
  <script src="js/contact_me.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/freelancer.min.js"></script>

</body>

</html>

<?php
  if (isset($_POST['enviar_solicitacao'])) {
    $id_solicitacao= $_POST['id_solicitacao'];

    $disciplina= 'nenhuma';
    $data_falta= '0000-00-00';
    $id_curso_server= $_SESSION['id_curso'];

    if (isset($_POST['materia'])) {
      $disciplina= $_POST['materia'];
    }
    if (isset($_POST['data_falta'])) {
      $data_falta= $_POST['data_falta'];
    }
    if (isset($_POST['id_curso'])) {
      $id_curso_server= $_POST['id_curso'];
    }

    $justificativa= $_POST['justificativa'];
    $id_curso_servidor= $id_curso_server;

    $formatosPermitidos = array("png","jpeg","jpg", "pdf");

    $extencao = mb_strtolower(pathinfo($_FILES['comprovante']['name'], PATHINFO_EXTENSION));

    if (in_array($extencao, $formatosPermitidos)) {
      $pasta= 'logica/arquivos/';
      $temporario= $_FILES['comprovante']['tmp_name'];
      $novoNome_arquivo= uniqid().".$extencao";
      if (move_uploaded_file($temporario, $pasta.$novoNome_arquivo)) {
        $Control -> realizar_solicitacao($id_solicitacao, $_SESSION['id'], $data_falta, strtoupper($justificativa), $novoNome_arquivo, $disciplina, $id_curso_servidor, $_SESSION['id_campus'], $_SESSION['id_instituto']);
      } else {
        modal('Não foi possível fazer o upload do arquivo','Tente novamente mais tarde ou entre em contato com o servidor responsável','641218', '0');
      }
    } else {
      modal('Formato inválido','Somente arquivos .png .jpg .jpeg e .pdf são aceitos','641218', '0');
    }
  }
?>
